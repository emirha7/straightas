var request = require('../src/node_modules/supertest')
var server = require('../src/bin/www')
var expect = require('../src/node_modules/chai').expect;
var assert = require('../src/node_modules/chai').assert;
var jwt_decode = require('../src/node_modules/jwt-decode');

var testUser = {
	email: 'test1@test.com' + randomString(),
	password: 'simpletest',
	password_repeat: 'simpletest',
	token: null
};

var adminUser = {
	email: 'admin@gmail.com',
	password: 'admin123',
	token: null
};

var urednikUser = {
	email: 'urednik@gmail.com',
	password: 'urednik123',
	token: null
};

var newUrednikEvent = {
	name: "New urednik event",
	organizer: "FRI",
	date: "2019-09-09",
	time: "12:34",
	description: "New awesome event"
};

var newPasswordObject = {
	oldpassword: 'simpletest',
	newpassword: 'simpletest2',
	newpassword_repeat: 'simpletest2'
};

var invalidPasswordObject = {
	oldpassword: 'simpletest1',
	newpassword: 'simpletest',
	newpassword_repea: 'simpletest2'
};

function randomString() {
	var str = "";
	for (var i = 0; i < 20; i++) {
		str += Math.ceil(Math.random() * 100);
	}
	return str;
}

/* TESTING */

before(function(done) {
	this.timeout(3500);
	console.log("Waiting 3000 ms to get database connection.");
	setTimeout(function() {
		done();
	}, 3000);
});

before(function(done) {
	this.timeout(3500);
	request(server)
		.post('/entry/login')
		.send(adminUser)
		.end(function(err, res) {
			if (err) {
				done(err);
			} else {
				expect(res.headers).to.have.own.property('set-cookie');
				adminUser.token = res.headers['set-cookie'].pop().split(';')[0].split('token=')[1];
				done();
			}
		});
});

before(function(done) {
	this.timeout(3500);
	request(server)
		.post('/entry/login')
		.send(urednikUser)
		.end(function(err, res) {
			if (err) {
				done(err);
			} else {
				expect(res.headers).to.have.own.property('set-cookie');
				urednikUser.token = res.headers['set-cookie'].pop().split(';')[0].split('token=')[1];
				done();
			}
		});
});

describe('Entry page', function() {
	it('GET /entry', function(done) {
		request(server)
			.get('/entry')
			.expect('Content-Type', "text/html; charset=utf-8")
			.expect(200, done);
	});

	it('Kreiranje novega računa POST /entry/register', function(done) {
		request(server)
			.post('/entry/register')
			.send(testUser)
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.headers).to.have.own.property('set-cookie');
					testUser.token = res.headers['set-cookie'].pop().split(';')[0].split('token=')[1];
					done();
				}
			});
	});
});

describe('Entry page second test', function() {
	describe('Testing invalid login attempt', function() {
		it('Invalid email field POST /entry/login', function(done) {
			request(server)
				.post('/entry/login')
				.send({
					email: 'testtest.com',
					password: 'simpletest'
				})
				.expect('Content-Type', "text/html; charset=utf-8")
				.expect(400, done);
		});

		it('Invalid fields POST /entry/login', function(done) {
			request(server)
				.post('/entry/login')
				.send({
					missingZwei: 'Dummy Test'
				})
				.expect('Content-Type', "text/html; charset=utf-8")
				.expect(400, done);
		});
	});

	describe('Attempting to get token for future reference', function() {
		it('Valid login POST /entry/login', function(done) {
			request(server)
				.post('/entry/login')
				.send(testUser)
				.end(function(err, res) {
					if (err) {
						done(err);
					} else {
						expect(res.headers).to.have.own.property('set-cookie');
						testUser.token = res.headers['set-cookie'].pop().split(';')[0].split('token=')[1];
						done();
					}
				});
		});
	});
});

describe('Settings page', function() {
	it('Invalid GET /entry/settings (no token -> error 403)', function(done) {
		request(server)
			.get('/entry/settings')
			.set('Cookie', 'token=' + 'missing token')
			.expect(403, done);
	});

	it('GET /entry/settings', function(done) {
		request(server)
			.get('/entry/settings')
			.set('Cookie', 'token=' + testUser.token)
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});

	it('Change password POST /entry/settings', function(done) {
		request(server)
			.post('/entry/settings')
			.send(newPasswordObject)
			.set('Cookie', 'token=' + testUser.token)
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});

	it('Invalid change password POST /entry/settings (no token)', function(done) {
		request(server)
			.post('/entry/settings')
			.send(invalidPasswordObject)
			.set('Cookie', 'token=' + 'missing')
			.expect(403, done);
	});

	it('Invalid change password POST /entry/settings (malformed fields)', function(done) {
		request(server)
			.post('/entry/settings')
			.send(invalidPasswordObject)
			.set('Cookie', 'token=' + testUser.token)
			.expect(400, done);
	});
});

describe('Home page', function() {
	describe('User redirected to homepage', function() {
		it('GET /home', function(done) {
			request(server)
				.get('/home')
				.set('Cookie', 'token=' + testUser.token)
				.end(function(err, res) {
					if (err) {
						done(err);
					} else {
						assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
						done();
					}
				});
		});
	});
});

describe('Map page', function() {
	it('GET /map', function(done) {
		request(server)
			.get('/map')
			.expect('Content-type', 'text/html; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	it('GET /api/maps', function(done) {
		request(server)
			.get('/api/maps')
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body.data));
					assert.isNotEmpty(res.body.data);
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});


describe('Bus page', function() {
	this.timeout(8000);
	it('GET /bus', function(done) {
		request(server)
			.get('/bus')
			.expect('Content-type', 'text/html; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});

	it('GET /api/bus', function(done) {
		request(server)
			.get('/api/bus/sisenska')
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Invalid GET /api/bus part 1', function(done) {
		request(server)
			.get('/api/bus/sisenska/99')
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Invalid GET /api/bus part 2', function(done) {
		request(server)
			.get('/api/bus/')
			.expect('Content-type', 'text/html; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isAbove(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Admin notification', function() {
	this.timeout(5000);
		it('Admin POST /api/adminMessage', function(done) {
		request(server)
			.post('/api/adminMessage')
			.send({message: "New test message"})
			.set('x-access-token', adminUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('User GET /api/adminMessage', function(done) {
		request(server)
			.get('/api/adminMessage')
			.set('x-access-token', testUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.property(res.body, "data");
					assert(Array.isArray(res.body.data));
					assert.lengthOf(res.body.data, 1, 'Contains one message');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Admin POST /home/adminMessage', function(done) {
		request(server)
			.post('/home/adminMessage')
			.send({message: "Message test"})
			.set('x-access-token', adminUser.token)
			.set('Cookie', 'token=' + adminUser.token)
			.expect('Content-type', 'text/plain; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

var publicEventId = "";

describe('Urednik managment', function() {
	this.timeout(5000);
	it('Urednik POST /api/publicEvents', function(done) {
		request(server)
			.post('/api/publicEvents')
			.send(newUrednikEvent)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Invalid user POST /api/publicEvents', function(done) {
		request(server)
			.post('/api/publicEvents')
			.send(newUrednikEvent)
			.set('x-access-token', testUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isAbove(res.statusCode, 400, 'Status code must be over 400 to pass.');
					done();
				}
			});
	});
	
	it('User GET /api/publicEvents', function(done) {
		request(server)
			.get('/api/publicEvents')
			.set('x-access-token', testUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isNotEmpty(res.body);
					publicEventId = res.body[0]["_id"];
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Urednik DELETE /api/publicEvents/:id', function(done) {
		request(server)
			.delete('/api/publicEvents/' + publicEventId)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Unknown user DELETE /api/publicEvents/:id', function(done) {
		request(server)
			.delete('/api/publicEvents/' + publicEventId)
			.set('x-access-token', 'missing')
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isAbove(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('User GET /events/public', function(done) {
		request(server)
			.get('/events/public')
			.set('Cookie', 'token=' + testUser.token)
			.expect('Content-type', "text/html; charset=utf-8")
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
			
	it('Urednik GET /events/preview', function(done) {
		request(server)
			.get('/events/preview')
			.set('Cookie', 'token=' + urednikUser.token)
			.expect('Content-type', "text/html; charset=utf-8")
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Urednik POST /events/add', function(done) {
		request(server)
			.post('/events/add')
			.send({ name: "Test name",
					organizer: "Test organizer",
					description: "Test description",
					date: "2019-03-03",
					time: "12:00"})
			.set('Cookie', 'token=' + urednikUser.token)
			.expect('Content-type', "text/plain; charset=utf-8")
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
		});
	});
});

var noteId = "";
var slotId = "";
var eventId = "";
var newNoteObj = {
	text: "Adding new note to base"
};
var newSlotObj = {
	title: "TPO",
	date: new Date()
};
var newEventObj = {
	title: "My event test",
	startTime: new Date(),
	endTime: new Date(),
	description: "Description event"
};

describe('Home - Try to add new items to database', function() {	
	it('Notes PUT /api/users/:idUser/note', function(done) {
		request(server)
			.put('/api/users/' + jwt_decode(urednikUser.token).id + "/note")
			.send(newNoteObj)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('_id');
					expect(res.body).to.have.property('text');
					noteId = res.body["_id"];
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Slot PUT /api/users/:idUser/slot', function(done) {
		request(server)
			.put('/api/users/' + jwt_decode(urednikUser.token).id + "/slot")
			.send(newSlotObj)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('_id');
					expect(res.body).to.have.property('title');
					expect(res.body).to.have.property('date');
					slotId = res.body["_id"];
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Event PUT /api/users/:idUser/event', function(done) {
		request(server)
			.put('/api/users/' + jwt_decode(urednikUser.token).id + "/event")
			.send(newEventObj)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isNotEmpty(res.body);
					expect(res.body[res.body.length - 1]).to.have.property('_id');
					eventId = res.body[res.body.length - 1]["_id"];
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Home - Try to get all items from database', function() {	
	it('Notes GET /api/users/:idUser/note', function(done) {
		request(server)
			.get('/api/users/' + jwt_decode(urednikUser.token).id + "/note")
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isNotEmpty(res.body);
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Slot GET /api/users/:idUser/slot', function(done) {
		request(server)
			.get('/api/users/' + jwt_decode(urednikUser.token).id + "/slot")
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isNotEmpty(res.body);
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Event GET /api/users/:idUser/event', function(done) {
		request(server)
			.get('/api/users/' + jwt_decode(urednikUser.token).id + "/event")
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isNotEmpty(res.body);
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Home - Try to get specific item from database', function() {	
	it('Event GET /api/users/:idUser/event/:id', function(done) {
		request(server)
			.get('/api/users/' + jwt_decode(urednikUser.token).id + "/event/" + eventId)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body.data).to.have.property('_id');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Home - Try to update item in database', function() {	
	it('Note update PUT /api/users/:idUser/note/:noteId', function(done) {
		request(server)
			.put('/api/users/' + jwt_decode(urednikUser.token).id + "/note/" + noteId)
			.send(newNoteObj)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('_id');
					expect(res.body).to.have.property('text');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Slot update PUT /api/users/:idUser/slot/:slotId', function(done) {
		request(server)
			.put('/api/users/' + jwt_decode(urednikUser.token).id + "/slot/" + slotId)
			.send(newSlotObj)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('_id');
					expect(res.body).to.have.property('title');
					expect(res.body).to.have.property('date');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Event update PUT /api/users/:idUser/event/:eventId', function(done) {
		request(server)
			.put('/api/users/' + jwt_decode(urednikUser.token).id + "/event/" + eventId)
			.send(newEventObj)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isNotEmpty(res.body);
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Home - Try to delete items in database', function() {	
	it('Note DELETE /api/users/:idUser/note/:noteId', function(done) {
		request(server)
			.delete('/api/users/' + jwt_decode(urednikUser.token).id + "/note/" + noteId)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('message');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Slot DELETE /api/users/:idUser/slot/:slotId', function(done) {
		request(server)
			.delete('/api/users/' + jwt_decode(urednikUser.token).id + "/slot/" + slotId)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('_id');
					expect(res.body).to.have.property('calendar');
					expect(res.body).to.have.property('todo');
					expect(res.body).to.have.property('timetable');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('Event DELETE /api/users/:idUser/event/:eventId', function(done) {
		request(server)
			.delete('/api/users/' + jwt_decode(urednikUser.token).id + "/event/" + eventId)
			.set('x-access-token', urednikUser.token)
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					expect(res.body).to.have.property('message');
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Public event page', function() {
	it('GET /events/public', function(done) {
		request(server)
			.get('/events/public')
			.expect('Content-type', "text/html; charset=utf-8")
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
	
	it('GET /publicEvents', function(done) {
		request(server)
			.get('/api/publicEvents')
			.expect('Content-type', 'application/json; charset=utf-8')
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert(Array.isArray(res.body));
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Error page', function() {
	it('GET /unknownPath', function(done) {
		request(server)
			.get('/unknownPath')
			.expect('Content-type', "text/html; charset=utf-8")
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isAbove(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});

describe('Entry redirect', function() {
	it('GET /', function(done) {
		request(server)
			.get('/')
			.expect('Content-type', "text/plain; charset=utf-8")
			.end(function(err, res) {
				if (err) {
					done(err);
				} else {
					assert.isBelow(res.statusCode, 400, 'Status code must be below 400 to pass.');
					done();
				}
			});
	});
});