var express = require('express');
var router = express.Router();
var VerifyToken = require("../helpers/verifyToken");
var ctrlUser = require('../controllers/user');

router.get('/', ctrlUser.login);
router.post('/login', ctrlUser.loginCheck);
router.post('/register', ctrlUser.registerCheck);
router.get('/settings', VerifyToken.verifyToken, ctrlUser.settings);
router.post('/settings', VerifyToken.verifyToken, ctrlUser.settingsCheck);
router.get('/logout', ctrlUser.logout);

module.exports = router;