var express = require('express');
var router = express.Router();
var ctrlHome = require('../controllers/home');
var VerifyToken = require("../helpers/verifyToken");

router.get('/', VerifyToken.verifyToken, ctrlHome.home);
router.post('/adminMessage', VerifyToken.verifyToken, ctrlHome.sendMessageToAll);

module.exports = router;