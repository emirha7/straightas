var express = require('express');
var router = express.Router();
var ctrlEvents = require('../controllers/public-events');
var VerifyToken = require("../helpers/verifyToken");

router.get('/preview', VerifyToken.verifyToken, ctrlEvents.previewEventsUpravljalec);
router.get('/public', ctrlEvents.allPublicEvents);
router.post('/add', VerifyToken.verifyToken, ctrlEvents.upravljalecAdd);

module.exports = router;