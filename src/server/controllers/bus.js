var CheckUpravljalec = require('../helpers/verifyToken');
var VerifyToken = require("../helpers/verifyToken");

// Server se spremeni za REST klice
var paramsApi = {
	server: 'http://localhost:3000',
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://tpo-ci-cd-team10.herokuapp.com';
}

// Render login page
module.exports.showBuses = function(req, res) {
	VerifyToken.isUserLoggedIn(req, function(bool){
		CheckUpravljalec.isUserUpravljalec(req, function(isUserUrednik){
			res.render('bus', {
				title: 'Trola',
				isUserUrednik: isUserUrednik,
				isUserLoggedIn: bool
			});
		});
	});
};