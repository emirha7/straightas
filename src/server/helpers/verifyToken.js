var jwt = require('jsonwebtoken');

module.exports.verifyToken = function(req, res, next) {
	var token = req.cookies['token'];
	if (!token) {
		res.redirect(403, '/entry');
		return;
	}
	jwt.verify(token, "mylittlesecret", function(err, decoded) {
		if (err) {
			res.redirect(403, '/entry');
			return;
		}
		next();
	});
}

module.exports.isUserLoggedIn = function(req, cb) {
	var token = req.cookies['token'];
	if (!token) {
		cb(false);
		return;
	}
	jwt.verify(token, "mylittlesecret", function(err, decoded) {
		if (err) {
			cb(false);
			return;
		}
		cb(true);
	});
}

module.exports.isUserAdmin = function(req, cb) {
	var token = req.cookies['token'];
	if (!token) {
		cb(false);
		return;
	}
	jwt.verify(token, "mylittlesecret", function(err, decoded) {
		if (err) {
			cb(false);
			return;
		}
		cb(decoded.role == "admin");
	});
}

module.exports.isUserUpravljalec = function(req, cb) {
	var token = req.cookies['token'];
	if (!token) {
		cb(false);
		return;
	}
	jwt.verify(token, "mylittlesecret", function(err, decoded) {
		if (err) {
			cb(false);
			return;
		}
		cb(decoded.role == "urednik");
	});
}