var MongoClient = require('mongodb').MongoClient;
var https = require('https');
var index = 0;

MongoClient.connect('mongodb://admin:admin123@ds349045.mlab.com:49045/baza1', function(err, client) {
	if (err) {
		throw err;
	}

	var db = client.db('baza1');

	const arr = [{
			"address": "Slovenske cesta 52, 1000 Ljubljana",
			"title": "3Roses",
			"cenaDoplacila": "1,87 EUR",
			"cenaObroka": "4,50 EUR"
		},
		{
			"address": "Trubarjeva cesta 40, 1000 Ljubljana",
			"title": "ABI FALAFEL",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Topniška 29a, 1000 Ljubljana",
			"title": "AMBIENT",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Leskoškova 9e, 1000 Ljubljana",
			"title": "Antaro",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Ribji trg 2, 1000 Ljubljana",
			"title": "AROMA, restavracija in pizzerija",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Kardeljeva ploščad 5, 1000 Ljubljana",
			"title": "AULA Cofe",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Kongresni trg 3, 1000 Ljubljana",
			"title": "Azijska restavracija Han",
			"cenaDoplacila": "3,67 EUR",
			"cenaObroka": "6,30 EUR"
		},
		{
			"address": "Gosposvetska cesta 4, 1000 Ljubljana",
			"title": "Azijska restavracija Tloft",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Tržaška cesta 200, 1000 Ljubljana",
			"title": "B2 - dostava",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Tržaška cesta 200, 1000 Ljubljana",
			"title": "BABICA",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Čopova 12, 1000 Ljubljana",
			"title": "Balkan Ekspres Roštilj",
			"cenaDoplacila": "2,57 EUR",
			"cenaObroka": "5,20 EUR"
		},
		{
			"address": "Šmartinska 152g, 1000 Ljubljana",
			"title": "Balkan Ekspress-dostava",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Šmartinska 152 g, 1000 Ljubljana",
			"title": "Balkan Express",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Baragova 16, 1000 Ljubljana",
			"title": "Bar ENKA",
			"cenaDoplacila": "1,50 EUR",
			"cenaObroka": "4,13 EUR"
		},
		{
			"address": "Dunajska cesta 106, 1000 Ljubljana",
			"title": "Barbado",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Koroška cesta 1, 2000 Maribor",
			"title": "Bella Italia",
			"cenaDoplacila": "0,87 EUR",
			"cenaObroka": "3,50 EUR"
		},
		{
			"address": "Kardeljeva ploščad 17, 1000 Ljubljana",
			"title": "BIGDIL Cofe",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Večna pot 111, 1000 Ljubljana",
			"title": "Biotehniška fakulteta, Menza BF",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Ižanska cesta 10, 1000 Ljubljana",
			"title": "Biotehniški izobraževalni center Ljubljana",
			"cenaDoplacila": "2,67 EUR",
			"cenaObroka": "5,30 EUR"
		},
		{
			"address": "Cesta v Mestni log 47, 1000 Ljubljana",
			"title": "Biotehniški izobraževalni center Ljubljana",
			"cenaDoplacila": "2,67 EUR",
			"cenaObroka": "5,30 EUR"
		},
		{
			"address": "Dunajska 101, 1000 Ljubljana",
			"title": "Bistro Suwon",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Kardeljeva ploščad 5, 1000 Ljubljana",
			"title": "Bistro Šumi",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Cankarjeva ulica 58, 9000 Murska Sobota",
			"title": "Bistro Tanja- DOSTAVA",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Vojkovo nabrežje 12, 6000 Koper/Capodistria",
			"title": "BISTRO VILLA DOMUS",
			"cenaDoplacila": "3,10 EUR",
			"cenaObroka": "5,73 EUR"
		},
		{
			"address": "Kidričeva ulica 23, 3250 Rogaška Slatina",
			"title": "Bohor",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Špelina ulica 17a, 2000 Maribor",
			"title": "Bowling center Strike",
			"cenaDoplacila": "2,87 EUR",
			"cenaObroka": "5,50 EUR"
		},
		{
			"address": "Razlagova 9, 2000 Maribor",
			"title": "Bruc bar",
			"cenaDoplacila": "1,50 EUR",
			"cenaObroka": "4,13 EUR"
		},
		{
			"address": "Ulica Pariške komune 37, 2000 Maribor",
			"title": "Cantante cafe Tabor",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Ulica Pariške komune 37, 2000 Maribor",
			"title": "Cantante cafe Tabor - DOSTAVA",
			"cenaDoplacila": "4,07 EUR",
			"cenaObroka": "6,70 EUR"
		},
		{
			"address": "Knafljev prehod 2, 1000 Ljubljana",
			"title": "CANTINA MEXICANA KNAFELJ",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Jurčkova cesta 223, 1000 Ljubljana",
			"title": "CANTINA MEXICANA RUDNIK",
			"cenaDoplacila": "4,20 EUR",
			"cenaObroka": "6,83 EUR"
		},
		{
			"address": "Trubarjeva cesta 23, 1000 Ljubljana",
			"title": "Centralna postaja",
			"cenaDoplacila": "3,77 EUR",
			"cenaObroka": "6,40 EUR"
		},
		{
			"address": "Verovškova 64, 1000 Ljubljana",
			"title": "CITY LUZ",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Ankaranska 7, 6000 Koper/Capodistria",
			"title": "CITYBURGER",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Knafljev prehod 1, 1000 Ljubljana",
			"title": "CUTTY FOOD CORNER",
			"cenaDoplacila": "1,37 EUR",
			"cenaObroka": "4,00 EUR"
		},
		{
			"address": "Tomažičev trg 1, 6000 Koper/Capodistria",
			"title": "Čevabdžinica Sarajevo 84",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Gramšijev trg 8, 6000 Koper/Capodistria",
			"title": "Čevabdžinica Sarajevo 84",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Šubičeva 1a, 1000 Ljubljana",
			"title": "DA BU DA, Azijska restavracija",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Borštnikov trg 3, 1000 Ljubljana",
			"title": "DAS IST WALTER Ljubljana Center",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Eipprova 11, 1000 Ljubljana",
			"title": "Dežela okusov",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Gortanova 21, 1000 Ljubljana",
			"title": "DIFOOD-eat good",
			"cenaDoplacila": "1,80 EUR",
			"cenaObroka": "4,43 EUR"
		},
		{
			"address": "Poljanska cesta 26, 1000 Ljubljana",
			"title": "Dijaški dom Ivana Cankarja",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Titova cesta 24a, 2000 Maribor",
			"title": "Dijaški dom Lizike Jančar",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Gosposvetska cesta 89, 2000 Maribor",
			"title": "Dijaški dom Maribor",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Potočnikova ulica 3, 1000 Ljubljana",
			"title": "Dijaški dom Poljane",
			"cenaDoplacila": "2,20 EUR",
			"cenaObroka": "4,83 EUR"
		},
		{
			"address": "Sončna pot 20, 6320 Portorož/Portorose",
			"title": "Dijaški dom Portorož",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Vidovdanska 7, 1000 Ljubljana",
			"title": "Dijaški dom Tabor",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Gerbičeva 51/a, 1000 Ljubljana",
			"title": "Dijaški dom Vič",
			"cenaDoplacila": "2,40 EUR",
			"cenaObroka": "5,03 EUR"
		},
		{
			"address": "Cankarjeva 5, 6000 Koper/Capodistria",
			"title": "Dijaški in študentski dom Koper",
			"cenaDoplacila": "2,40 EUR",
			"cenaObroka": "5,03 EUR"
		},
		{
			"address": "Šegova ulica 115, 8000 Novo mesto",
			"title": "Dijaški in študentski dom Novo mesto",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Celovška cesta 69 a, 1000 Ljubljana",
			"title": "Dobrote vzhoda",
			"cenaDoplacila": "3,60 EUR",
			"cenaObroka": "6,23 EUR"
		},
		{
			"address": "Dunajska cesta 113, 1000 Ljubljana",
			"title": "Domača pekarna",
			"cenaDoplacila": "0,70 EUR",
			"cenaObroka": "3,33 EUR"
		},
		{
			"address": "Trg OF 13, 1000 Ljubljana",
			"title": "Dunya doner",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Strossmayerjeva 13, 2000 Maribor",
			"title": "Eat smart",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Smetanova 17, 2000 Maribor",
			"title": "Eat smart FERI",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Cesta maršala Tita 27, 4270 Jesenice",
			"title": "Ejga - restavracija - kavarna - pub - catering",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Slovenska cesta 10, 1000 Ljubljana",
			"title": "EKSPERIMENT restavracija &amp; bar",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Poljanska cesta 22, 1000 Ljubljana",
			"title": "Fajn Fuud",
			"cenaDoplacila": "2,80 EUR",
			"cenaObroka": "5,43 EUR"
		},
		{
			"address": "Poljanska cesta 22, 1000 Ljubljana",
			"title": "Fajn Fuud Pizza",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Tržaška cesta 25, 1000 Ljubljana",
			"title": "Fakulteta za elektrotehniko, Menza FE",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Slovenska 51, 1000 Ljubljana",
			"title": "FALAFEL HIŠA",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Slovenska 51, 1000 Ljubljana",
			"title": "FALAFEL HIŠA-DOSTAVA",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Miklošičeva 34, 1000 Ljubljana",
			"title": "Fari's Delice",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Slovenska cesta 51, 1000 Ljubljana",
			"title": "Fast food Fresco in Slast",
			"cenaDoplacila": "1,50 EUR",
			"cenaObroka": "4,13 EUR"
		},
		{
			"address": "Pristaniška ulica2, 6000 Koper/Capodistria",
			"title": "Fast food MAGIC",
			"cenaDoplacila": "2,00 EUR",
			"cenaObroka": "4,63 EUR"
		},
		{
			"address": "Trg OF 13, 1000 Ljubljana",
			"title": "Fastfood Ajda",
			"cenaDoplacila": "2,10 EUR",
			"cenaObroka": "4,73 EUR"
		},
		{
			"address": "Petkovškovo nabrežje 25, 1000 Ljubljana",
			"title": "Forum",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Vetrinjska ulica 9, 2000 Maribor",
			"title": "Fresh 4 life Vetrinjska",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Šolska cesta 2, 4220 Škofja Loka",
			"title": "Garač - Restavracija \"M\"",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Gosposvetska cesta 83, 2000 Maribor",
			"title": "Gosposvetska",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Šmihel 14, 8000 Novo mesto",
			"title": "Gostilna Bučar",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Cesta zmage 34, 1410 Zagorje ob Savi",
			"title": "Gostilna Čuk",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Njegoševa 6 K, 1000 Ljubljana",
			"title": "Gostilna in picerija Pavon",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Jurčkova cesta 7, 1000 Ljubljana",
			"title": "Gostilna in pizzeria Jurček",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Dolenjska cesta 103, 1000 Ljubljana",
			"title": "Gostilna JAKA",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Jurčkova cesta 99, 1000 Ljubljana",
			"title": "Gostilna Jerinček",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Trg Svobode 4, 2390 Ravne na Koroškem",
			"title": "Gostilna Lečnik",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Ormoška cesta 18, 2250 Ptuj",
			"title": "Gostilna Lužnik",
			"cenaDoplacila": "2,40 EUR",
			"cenaObroka": "5,03 EUR"
		},
		{
			"address": "Oplotniška cesta 5, 3210 Slovenske Konjice",
			"title": "Gostilna Pod Škalcami",
			"cenaDoplacila": "1,00 EUR",
			"cenaObroka": "3,63 EUR"
		},
		{
			"address": "Oplotniška cesta 5, 3210 Slovenske Konjice",
			"title": "Gostilna pod Škalcami - DOSTAVA",
			"cenaDoplacila": "1,00 EUR",
			"cenaObroka": "3,63 EUR"
		},
		{
			"address": "Brezina 19/b, 8250 Brežice",
			"title": "Gostilna Racman",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Goliev trg 8, 8210 Trebnje",
			"title": "Gostilna Šeligo",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Vodnikov trg 4, 2000 Maribor",
			"title": "Gostilna Zlati lev",
			"cenaDoplacila": "2,40 EUR",
			"cenaObroka": "5,03 EUR"
		},
		{
			"address": "Vodnikov trg 4, 2000 Maribor",
			"title": "Gostilna Zlati lev - DOSTAVA",
			"cenaDoplacila": "2,80 EUR",
			"cenaObroka": "5,43 EUR"
		},
		{
			"address": "Osojnikova 20a, 2000 Maribor",
			"title": "Gostilnica &amp; picerija Pobreški hram",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Rožna dolina, Cesta I. 26A, 1000 Ljubljana",
			"title": "Gostilnica in pivnica Katrca",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Jurčkova 225, 1000 Ljubljana",
			"title": "Gostilnica in pizzerija Kratochwill",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Šmartinska 152, 1000 Ljubljana",
			"title": "Gostilnica in pizzerija Kratochwill",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Slivniška cesta 6, 2311 Hoče",
			"title": "Gostinske storitve Michel Toš s.p.",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Slivniška cesta 6, 2311 Hoče",
			"title": "Gostinske storitve Michel Toš s.p. - DOSTAVA",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Prešernova cesta 1, 1410 Zagorje ob Savi",
			"title": "Gostišče KUM",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Glavni trg 30, 8000 Novo mesto",
			"title": "Gostišče na trgu - Hiša kulinarike",
			"cenaDoplacila": "2,77 EUR",
			"cenaObroka": "5,40 EUR"
		},
		{
			"address": "Zagrebška cesta 92, 2000 Maribor",
			"title": "Gurmanski hram",
			"cenaDoplacila": "2,20 EUR",
			"cenaObroka": "4,83 EUR"
		},
		{
			"address": "Zagrebška cesta 92, 2000 Maribor",
			"title": "Gurmanski hram - DOSTAVA",
			"cenaDoplacila": "2,20 EUR",
			"cenaObroka": "4,83 EUR"
		},
		{
			"address": "Litijska 28, 1000 Ljubljana",
			"title": "Halo Honkong - dostava",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Njegoševa 6K, 1000 Ljubljana",
			"title": "HALO KATRA",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Pod ježami 14, 1000 Ljubljana",
			"title": "Halo Pinki",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Pod ježami 14, 1000 Ljubljana",
			"title": "Halo Pinki - dostava",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Tržaška 116, 1000 Ljubljana",
			"title": "Halo Rdeče jabolko - dostava",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Hacquetova 5, 1000 Ljubljana",
			"title": "Halo Shaolin",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Cesta Ljubljanske brigade 33, 1000 Ljubljana",
			"title": "Hipermarket Ljubljana Šiška",
			"cenaDoplacila": "2,36 EUR",
			"cenaObroka": "4,99 EUR"
		},
		{
			"address": "Streliška ulica 10, 1000 Ljubljana",
			"title": "Hiša pod gradom",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Čopova 4, 1000 Ljubljana",
			"title": "Hit wok",
			"cenaDoplacila": "2,57 EUR",
			"cenaObroka": "5,20 EUR"
		},
		{
			"address": "Cesta v mestni log 55, 1000 Ljubljana",
			"title": "Hong Xiang",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Cesta v mestni log 55, 1000 Ljubljana",
			"title": "Hong Xiang - dostava",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Dilančeva 1, 8000 Novo mesto",
			"title": "Hostel Situla",
			"cenaDoplacila": "3,40 EUR",
			"cenaObroka": "6,03 EUR"
		},
		{
			"address": "Šmartinska cesta 152, 1000 Ljubljana",
			"title": "HOT HORSE - BTC",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Celovška cesta 25, 1000 Ljubljana",
			"title": "HOT HORSE - Tivoli",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Mariborska cesta 3, 3000 Celje",
			"title": "Hotel Celeia",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Goliev trg 13, 8210 Trebnje",
			"title": "Hotel Opara",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Rudarska cesta 1, 3320 Velenje",
			"title": "Hotel Paka",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Glavni trg 43, 2380 Slovenj Gradec",
			"title": "Hotel Slovenj Gradec",
			"cenaDoplacila": "1,87 EUR",
			"cenaObroka": "4,50 EUR"
		},
		{
			"address": "Kardeljeva ploščad 17, 1000 Ljubljana",
			"title": "HotSpot bar&amp;bistro",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Nazorjeva 6, 1000 Ljubljana",
			"title": "HotSpot Center bar&amp;bistro",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Tržaška cesta 5, 1000 Ljubljana",
			"title": "Italijanska restavracija Mirje",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Šubičeva 1, 1000 Ljubljana",
			"title": "Jet bar",
			"cenaDoplacila": "3,40 EUR",
			"cenaObroka": "6,03 EUR"
		},
		{
			"address": "Cankarjeva cesta 6, 1000 Ljubljana",
			"title": "Joe Pena's Cantina y Bar",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Aškrčeva 2, 1000 Ljubljana",
			"title": "K16",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Pivovarniška ulica 6, 1000 Ljubljana",
			"title": "KAMPUS PIZZA",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Gosposvetska cesta 15, 2000 Maribor",
			"title": "Kanape",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Kardeljeva ploščad , -1 Ljubljana",
			"title": "KAPITAL",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Drulovka 40, 4000 Kranj",
			"title": "Karantanija",
			"cenaDoplacila": "2,40 EUR",
			"cenaObroka": "5,03 EUR"
		},
		{
			"address": "Borštnikov trg 2, 1000 Ljubljana",
			"title": "Kavarna BORŠTNIK",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Delpinova 16, 5000 Nova Gorica",
			"title": "Kavarna in restavracija Pecivo",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Smetanova ulica 17, 2000 Maribor",
			"title": "Kavarna in sendvičarna Feri",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Gregorčičeva 2, 4000 Kranj",
			"title": "Kavarna in slaščičarna Carniola",
			"cenaDoplacila": "2,27 EUR",
			"cenaObroka": "4,90 EUR"
		},
		{
			"address": "Oprešnikova 28, 4000 Kranj",
			"title": "Kavarna in slaščičarna Rendez - Vous",
			"cenaDoplacila": "2,27 EUR",
			"cenaObroka": "4,90 EUR"
		},
		{
			"address": "Turjaška 1, 1000 Ljubljana",
			"title": "Kavarna NUK",
			"cenaDoplacila": "2,80 EUR",
			"cenaObroka": "5,43 EUR"
		},
		{
			"address": "Koroška cesta 59, 2000 Maribor",
			"title": "Kavarna Stil Burger",
			"cenaDoplacila": "1,37 EUR",
			"cenaObroka": "4,00 EUR"
		},
		{
			"address": "Zdravstevna  pot 5, 1000 Ljubljana",
			"title": "Kavarna STRIT",
			"cenaDoplacila": "1,90 EUR",
			"cenaObroka": "4,53 EUR"
		},
		{
			"address": "Focheva ulica 41, 2000 Maribor",
			"title": "Kitajska palača - DOSTAVA",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Ptujska cesta 131 , 2000 Maribor",
			"title": "Kitajska restavracija Azija",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Gosposvetska ulica 1, 4000 Kranj",
			"title": "Kitajska restavracija AZIJA",
			"cenaDoplacila": "3,20 EUR",
			"cenaObroka": "5,83 EUR"
		},
		{
			"address": "Masarykova 17, 1000 Ljubljana",
			"title": "Kitajska restavracija Beli labod",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Ankaranska cesta 5B, 6000 Koper/Capodistria",
			"title": "Kitajska restavracija Cesarska hiša",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Kersnikova 31, 3000 Celje",
			"title": "Kitajska restavracija Dva zmaja",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Šmartinska 152g, 1000 Ljubljana",
			"title": "Kitajska restavracija Han",
			"cenaDoplacila": "3,67 EUR",
			"cenaObroka": "6,30 EUR"
		},
		{
			"address": "Ižanska cesta 282, 1000 Ljubljana",
			"title": "Kitajska restavracija Kitajski zmaj",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Mačkovec 6, 8000 Novo mesto",
			"title": "Kitajska restavracija NANKING",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Kapitelska 5, 1000 Ljubljana",
			"title": "Kitajska restavracija Novi Šanghai",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Delavska ulica 11, 4270 Jesenice",
			"title": "Kitajska restavracija Šang Hai",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Ulica Pohorskega bataljona 14, 2000 Maribor",
			"title": "Kitajska restavracija Zlata srna",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Ulica Pohorskega bataljona 14, 2000 Maribor",
			"title": "Kitajska restavracija Zlata srna - DOSTAVA",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Loška ulica 10, 2000 Maribor",
			"title": "Kitajska restavracija Zvezda",
			"cenaDoplacila": "2,40 EUR",
			"cenaObroka": "5,03 EUR"
		},
		{
			"address": "Ankaranska cesta 5B, 6000 Koper/Capodistria",
			"title": "Kitajska restavracja Cesarska hiša-DOSTAVA",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Dunajska cesta 29, 1000 Ljubljana",
			"title": "Kitajski dvor",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Teharska cesta 35, 3000 Celje",
			"title": "Kitajski dvor",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Dunajska cesta 29, 1000 Ljubljana",
			"title": "Kitajski dvor - dostava",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Teharska cesta 35, 3000 Celje",
			"title": "Kitajski dvor - DOSTAVA",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Slamnikarjeva 1, 1230 Domžale",
			"title": "Kitajsko mesto",
			"cenaDoplacila": "2,87 EUR",
			"cenaObroka": "5,50 EUR"
		},
		{
			"address": "Slamnikarjeva 1, 1230 Domžale",
			"title": "Kitajsko mesto - dostava",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Zaloška cesta 7, 1000 Ljubljana",
			"title": "Klinika bar",
			"cenaDoplacila": "1,30 EUR",
			"cenaObroka": "3,93 EUR"
		},
		{
			"address": "Litostrojska cesta 57, 1000 Ljubljana",
			"title": "Klopčič",
			"cenaDoplacila": "2,77 EUR",
			"cenaObroka": "5,40 EUR"
		},
		{
			"address": "Litostrojska cesta 55, 1000 Ljubljana",
			"title": "Klopčič - Dom Litostroj",
			"cenaDoplacila": "2,77 EUR",
			"cenaObroka": "5,40 EUR"
		},
		{
			"address": "Nemčavci 1d, 9000 Murska Sobota",
			"title": "Kratochwill",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Tbilisijska 59, 1000 Ljubljana",
			"title": "Kurji tat",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Ulica Staneta Žagarja 2b, 4240 Radovljica",
			"title": "LA CANTINA",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Čevljarska 9, 6000 Koper/Capodistria",
			"title": "Labirint",
			"cenaDoplacila": "3,10 EUR",
			"cenaObroka": "5,73 EUR"
		},
		{
			"address": "Kvedrova cesta 5a, 1000 Ljubljana",
			"title": "Leteča zvezda",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Borštnikov trg 3a, 1000 Ljubljana",
			"title": "LIPCA - INDEKS",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Zaloška cesta 42, 1000 Ljubljana",
			"title": "Ljudska kuhinja",
			"cenaDoplacila": "2,27 EUR",
			"cenaObroka": "4,90 EUR"
		},
		{
			"address": "Stossmayerjeva ulica 26, 2000 Maribor",
			"title": "London Street Food",
			"cenaDoplacila": "3,40 EUR",
			"cenaObroka": "6,03 EUR"
		},
		{
			"address": "Gosposvetska cesta 43a, 2000 Maribor",
			"title": "Loving Hut",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Trg OF 14, 1000 Ljubljana",
			"title": "Loving Hut Ljubjana Center",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Gosposvetska cesta 19, 2000 Maribor",
			"title": "Lunch box",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Mariborska 100, 3000 Celje",
			"title": "Maestro (City center Celje)",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Vodnikova 35, 1000 Ljubljana",
			"title": "Maharaja indijska restavracija",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Trg republike 1, 1000 Ljubljana",
			"title": "Maxi Ljubljana",
			"cenaDoplacila": "2,36 EUR",
			"cenaObroka": "4,99 EUR"
		},
		{
			"address": "Trg republike 1, 1000 Ljubljana",
			"title": "Maxi Restavracija Romansa 1971",
			"cenaDoplacila": "4,27 EUR",
			"cenaObroka": "6,90 EUR"
		},
		{
			"address": "Ulica Vita Kraigherja 5, 2000 Maribor",
			"title": "MC PANDA",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Žička cesta 4a, 3210 Slovenske Konjice",
			"title": "MC Patriot",
			"cenaDoplacila": "1,00 EUR",
			"cenaObroka": "3,63 EUR"
		},
		{
			"address": "Tržaška 6, 2000 Maribor",
			"title": "McDonald´s restavracija - Swaty",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Pobreška 18, 2000 Maribor",
			"title": "McDonald´s restavracija Europark",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Ptujska 106, 2000 Maribor",
			"title": "McDonald´s restavracija Ptujska",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Kidričeva 2b, 3320 Velenje",
			"title": "McDonald´s restavracija Velenje",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Breznikova 15, 1230 Domžale",
			"title": "McDonald's McDrive Domžale",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Mariborska cesta 164, 3000 Celje",
			"title": "McDonald's restavracija - Celje Drive",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Celovška cesta 170, 1000 Ljubljana",
			"title": "McDonald's restavracija - Celovška",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Cesta Boštjana Hladnika 7, 4000 Kranj",
			"title": "McDonald's restavracija - Kranj Drive",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Ankaranska 4, 6000 Koper/Capodistria",
			"title": "McDonald's restavracija Koper",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Vojkova cesta 111, 5000 Nova Gorica",
			"title": "McDonald's restavracija Nova Gorica",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Jezdarska ulica 8b, 2000 Maribor",
			"title": "ME GUSTA",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Mlinska ulica 2, 2000 Maribor",
			"title": "Meating pub &amp; restavracija",
			"cenaDoplacila": "3,40 EUR",
			"cenaObroka": "6,03 EUR"
		},
		{
			"address": "Ciril Metodov trg 16, 1000 Ljubljana",
			"title": "MEDITERRANEO",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Ulica Jake Platiše 17, 4000 Kranj",
			"title": "Mehiška restavracija Cantina Maria",
			"cenaDoplacila": "2,80 EUR",
			"cenaObroka": "5,43 EUR"
		},
		{
			"address": "Šmartinska 152g, 1000 Ljubljana",
			"title": "Mehiška restavracija Imperio - dostava",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Šmartinska 152g, 1000 Ljubljana",
			"title": "Mehiška restavracija Imperio mexicano",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Celovška cesta 166, 1000 Ljubljana",
			"title": "MENZA",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Aškerčeva cesta 6, 1000 Ljubljana",
			"title": "Menza FS",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Rimska cesta 25a, 1000 Ljubljana",
			"title": "Meta &amp; Bazilika",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Maistrov trg 13, 4000 Kranj",
			"title": "Mlečna restavracija",
			"cenaDoplacila": "2,27 EUR",
			"cenaObroka": "4,90 EUR"
		},
		{
			"address": "Maistrov trg 13, 4000 Kranj",
			"title": "Mlečna restavracija - DOSTAVA",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Pistaniška ulica 2, 6000 Koper/Capodistria",
			"title": "Mornarček ribja kantina - restavracija",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Trubarjeva ulica 31, 1000 Ljubljana",
			"title": "Namaste Indian Express",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Obala 4F, 6320 Portorož/Portorose",
			"title": "News Cafe",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Tyrševa 3, 2000 Maribor",
			"title": "Noomm",
			"cenaDoplacila": "3,60 EUR",
			"cenaObroka": "6,23 EUR"
		},
		{
			"address": "Cankarjeva ulica 58, 9000 Murska Sobota",
			"title": "Okrepčevalnica - bistro Tanja",
			"cenaDoplacila": "2,37 EUR",
			"cenaObroka": "5,00 EUR"
		},
		{
			"address": "Linhartova 11, 1000 Ljubljana",
			"title": "Okrepčevalnica - Diner kino gledališče Bežigrad",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Dunajska cesta 111, 1000 Ljubljana",
			"title": "Okrepčevalnica - pizzerija Maks",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Trubarjeva cesta 36, 1000 Ljubljana",
			"title": "Okrepčevalnica \"Riviera\"",
			"cenaDoplacila": "2,60 EUR",
			"cenaObroka": "5,23 EUR"
		},
		{
			"address": "Ajdovščina 4, 1000 Ljubljana",
			"title": "Okrepčevalnica Ajda",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Nove Fužine 43, 1000 Ljubljana",
			"title": "Okrepčevalnica AZZURRO",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Kardeljeva ploščad 5, 1000 Ljubljana",
			"title": "Okrepčevalnica Badovinac",
			"cenaDoplacila": "1,20 EUR",
			"cenaObroka": "3,83 EUR"
		},
		{
			"address": "Partizanska 50, 2000 Maribor",
			"title": "Okrepčevalnica Bober (Partizanska)",
			"cenaDoplacila": "2,00 EUR",
			"cenaObroka": "4,63 EUR"
		},
		{
			"address": "Poljanska cesta 7, 1000 Ljubljana",
			"title": "Okrepčevalnica BUREK",
			"cenaDoplacila": "1,20 EUR",
			"cenaObroka": "3,83 EUR"
		},
		{
			"address": "Prešernova 17, 2000 Maribor",
			"title": "Okrepčevalnica IZUM",
			"cenaDoplacila": "2,87 EUR",
			"cenaObroka": "5,50 EUR"
		},
		{
			"address": "Hajdrihova 19, 1000 Ljubljana",
			"title": "Okrepčevalnica Kemijski inštitut",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Pivola 10, 2311 Hoče",
			"title": "Okrepčevalnica Meranovo",
			"cenaDoplacila": "2,00 EUR",
			"cenaObroka": "4,63 EUR"
		},
		{
			"address": "Celjska cesta 48 b, 2380 Slovenj Gradec",
			"title": "Okrepčevalnica Minutka",
			"cenaDoplacila": "2,37 EUR",
			"cenaObroka": "5,00 EUR"
		},
		{
			"address": "Koroška cesta 9, 2000 Maribor",
			"title": "Okrepčevalnica MM PANDA",
			"cenaDoplacila": "2,00 EUR",
			"cenaObroka": "4,63 EUR"
		},
		{
			"address": "Ulica Vita Kraigherja 5, 2000 Maribor",
			"title": "Okrepčevalnica Noodle box",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Koštialova 1, 8000 Novo mesto",
			"title": "Okrepčevalnica Novak",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Kotnikova 5, 1000 Ljubljana",
			"title": "Okrepčevalnica Pekarna Dobrot",
			"cenaDoplacila": "1,40 EUR",
			"cenaObroka": "4,03 EUR"
		},
		{
			"address": "Koroška cesta 5, 2000 Maribor",
			"title": "Okrepčevalnica Pivnica Alf",
			"cenaDoplacila": "2,80 EUR",
			"cenaObroka": "5,43 EUR"
		},
		{
			"address": "Koroška cesta 5, 2000 Maribor",
			"title": "Okrepčevalnica Pivnica Alf - DOSTAVA",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Adamič Lundrovo nabrežje 1, 1000 Ljubljana",
			"title": "Okrepčevalnica Ribca",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Gosposvetska cesta 43c, 2000 Maribor",
			"title": "Okrepčevalnica Sarajevo",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Glavni trg 5, 2000 Maribor",
			"title": "Okrepčevalnica Stari Gril",
			"cenaDoplacila": "2,80 EUR",
			"cenaObroka": "5,43 EUR"
		},
		{
			"address": "Usnjarska ulica 5, 2000 Maribor",
			"title": "Okrepčevalnica ŠIŠ",
			"cenaDoplacila": "3,29 EUR",
			"cenaObroka": "5,92 EUR"
		},
		{
			"address": "Kardeljeva ploščad 5, 1000 Ljubljana",
			"title": "Okrepčevalnica Šumi",
			"cenaDoplacila": "1,00 EUR",
			"cenaObroka": "3,63 EUR"
		},
		{
			"address": "Trg OF 10, 1000 Ljubljana",
			"title": "Okrepčevalnica TimeOut",
			"cenaDoplacila": "1,20 EUR",
			"cenaObroka": "3,83 EUR"
		},
		{
			"address": "Partizanska cesta 50, 2000 Maribor",
			"title": "Okrepčevalnica TIP TOP",
			"cenaDoplacila": "1,00 EUR",
			"cenaObroka": "3,63 EUR"
		},
		{
			"address": "Spuhlja 32, 2250 Ptuj",
			"title": "Okrepčevalnica Villa Monde",
			"cenaDoplacila": "2,67 EUR",
			"cenaObroka": "5,30 EUR"
		},
		{
			"address": "Ulica Eve Lovše 1, 2000 Maribor",
			"title": "Oštarija Pizzeria Da Noi",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Ulica Eve Lovše 1, 2000 Maribor",
			"title": "Oštarija Pizzeria Da Noi - DOSTAVA",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Šmartinska cesta 152, 1000 Ljubljana",
			"title": "Oštarija pri Oljki",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Kandijska cesta 35, 8000 Novo mesto",
			"title": "Oštarija Rudolfswerth",
			"cenaDoplacila": "2,37 EUR",
			"cenaObroka": "5,00 EUR"
		},
		{
			"address": "Gosposka ulica 6, 2000 Maribor",
			"title": "Papagayo",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Šubičeva 1, 1000 Ljubljana",
			"title": "Parlament pub Surf´n´fries",
			"cenaDoplacila": "2,30 EUR",
			"cenaObroka": "4,93 EUR"
		},
		{
			"address": "Ljubljanska cesta 21, 3000 Celje",
			"title": "PE Dijaški dom Celje",
			"cenaDoplacila": "2,54 EUR",
			"cenaObroka": "5,17 EUR"
		},
		{
			"address": "Glavni trg 41, 2380 Slovenj Gradec",
			"title": "PE Kava bar in okrepčevalnica Fantom",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Tobačna ulica 5, 1000 Ljubljana",
			"title": "PE Marjetica",
			"cenaDoplacila": "2,27 EUR",
			"cenaObroka": "4,90 EUR"
		},
		{
			"address": "Glavni trg 25, 2380 Slovenj Gradec",
			"title": "Pek Matjaž Glavni trg",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Tomšičeva cesta 15, 3320 Velenje",
			"title": "Pek Matjaž Tomšičeva",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Jurčkova cesta 223, 1000 Ljubljana",
			"title": "Piazza del Papa",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Ferrarska 14, 6000 Koper/Capodistria",
			"title": "Picerija Alan Ford",
			"cenaDoplacila": "3,07 EUR",
			"cenaObroka": "5,70 EUR"
		},
		{
			"address": "Cesta v Mestni log 55, 1000 Ljubljana",
			"title": "Picerija Barjan",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Celjska cesta 7, 3310 Žalec",
			"title": "Picerija Bonitas",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Pod Trško goro 83, 8000 Novo mesto",
			"title": "Picerija ERA",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Otoška cesta 5, 8000 Novo mesto",
			"title": "Picerija Lavanda",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Meljska cesta 39, 2000 Maribor",
			"title": "Picerija restavracija Moriccu",
			"cenaDoplacila": "2,87 EUR",
			"cenaObroka": "5,50 EUR"
		},
		{
			"address": "UlicaIvana Roba 56, 8000 Novo mesto",
			"title": "Picerija Štangelj",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Opekarniška cesta 9, 3000 Celje",
			"title": "Picerija Verona",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Opekarniška cesta 9, 3000 Celje",
			"title": "Picerija Verona - DOSTAVA",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Celovška cesta 249, 1000 Ljubljana",
			"title": "Picestavracija Boccaccio",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Perkova 17, 1230 Domžale",
			"title": "Pivnica Adam Ravbar",
			"cenaDoplacila": "3,77 EUR",
			"cenaObroka": "6,40 EUR"
		},
		{
			"address": "Kolodvorska ulica 14, 1000 Ljubljana",
			"title": "Pivovarna in pivnica Kratochwill",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Ljubljanska cesta 5, 4260 Bled",
			"title": "Pizzeria Briksen",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Rozmanova ulica 25, 8000 Novo mesto",
			"title": "Pizzeria Chianti",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Ulica Eve Lovše 1, 2000 Maribor",
			"title": "Pizzeria Da noi fast food",
			"cenaDoplacila": "0,60 EUR",
			"cenaObroka": "3,23 EUR"
		},
		{
			"address": "Gregorčičeva ulica 3, 1000 Ljubljana",
			"title": "Pizzeria FoculuS",
			"cenaDoplacila": "4,30 EUR",
			"cenaObroka": "6,93 EUR"
		},
		{
			"address": "Dalmatinova ulica 2, 8270 Krško",
			"title": "Pizzeria Fontana",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Vojkova 34, 1000 Ljubljana",
			"title": "Pizzeria Gregorino",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Eipprova ulica 17, 1000 Ljubljana",
			"title": "Pizzeria in restavracija Trnovski zvon",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Pobreška cesta 18, 2000 Maribor",
			"title": "Pizzeria in špageteria Al Capone",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Šmartinska cesta 152, 1000 Ljubljana",
			"title": "Pizzeria in špageterija AL CAPONE",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Podbevškova ulica 11, 8000 Novo mesto",
			"title": "Pizzeria in špagetteria Cubus",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Tržaška 79a, 1000 Ljubljana",
			"title": "Pizzeria Laterna",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Dečkova cesta 41, 3000 Celje",
			"title": "Pizzeria OLIVA",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Dečkova cesta 41, 3000 Celje",
			"title": "Pizzeria OLIVA - dostava",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Nazorjeva ulica 8, 1000 Ljubljana",
			"title": "Pizzeria Osmica",
			"cenaDoplacila": "3,40 EUR",
			"cenaObroka": "6,03 EUR"
		},
		{
			"address": "Trg republike 2, 1000 Ljubljana",
			"title": "Pizzeria Parma",
			"cenaDoplacila": "3,80 EUR",
			"cenaObroka": "6,43 EUR"
		},
		{
			"address": "Koroška cesta 60, 2000 Maribor",
			"title": "Pizzeria Pronto",
			"cenaDoplacila": "2,20 EUR",
			"cenaObroka": "4,83 EUR"
		},
		{
			"address": "Miklošičeva cesta 22, 1000 Ljubljana",
			"title": "Pizzeria Šestinka",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Kandijska cesta 60, 8000 Novo mesto",
			"title": "Pizzeria Totalka",
			"cenaDoplacila": "3,40 EUR",
			"cenaObroka": "6,03 EUR"
		},
		{
			"address": "Karlovška cesta 5, 1000 Ljubljana",
			"title": "Pizzeria Tunnel",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Tomažičeva ulica 10, 6310 Izola/Isola",
			"title": "Pizzeria Vila Raineri",
			"cenaDoplacila": "3,27 EUR",
			"cenaObroka": "5,90 EUR"
		},
		{
			"address": "Pečarjeva 1, 1410 Zagorje ob Savi",
			"title": "Pizzerija Ašič",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Pečarjeva ulica 1, 1410 Zagorje ob Savi",
			"title": "Pizzerija Ašič - dostava",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Čevljarska 8, 6000 Koper/Capodistria",
			"title": "Pizzerija Atrij",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Obrtniška cesta 1, 1420 Trbovlje",
			"title": "Pizzerija Dimnik",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Obrtniška cesta 1, 1420 Trbovlje",
			"title": "Pizzerija Dimnik - dostava",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Ulica Marije Hvaličeve 6, 1000 Ljubljana",
			"title": "Pizzerija Etna - italijanska kuhinja",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Koroška cesta 59, 4000 Kranj",
			"title": "Pizzerija Gorenc",
			"cenaDoplacila": "2,77 EUR",
			"cenaObroka": "5,40 EUR"
		},
		{
			"address": "Cesta v Mestni log 55, 1000 Ljubljana",
			"title": "Pizzerija in okrepčevalnica KONDOR",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Stara cesta 25b, 4000 Kranj",
			"title": "Pizzerija in špageterija BUF Kranj",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Vojkova 71, 1000 Ljubljana",
			"title": "Pizzerija Piramida",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Šalek 24 a, 3320 Velenje",
			"title": "Pizzerija Velun",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Dravska ulica 8, 2000 Maribor",
			"title": "Pizzerija VERDI",
			"cenaDoplacila": "2,57 EUR",
			"cenaObroka": "5,20 EUR"
		},
		{
			"address": "Kardeljeva ploščad 17, 1000 Ljubljana",
			"title": "Podmornica",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Pot pomorščakov 4, 6320 Portorož/Portorose",
			"title": "Pomorščak",
			"cenaDoplacila": "1,90 EUR",
			"cenaObroka": "4,53 EUR"
		},
		{
			"address": "Poljanski nasip 2, 1000 Ljubljana",
			"title": "Pravna fakulteta",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Vodovodna cesta 90, 1000 Ljubljana",
			"title": "PREHRANA Bar VO-KA",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Dunajska 160, 1000 Ljubljana",
			"title": "Prehrana Palača SMELT",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Prešernova cesta 55, 6310 Izola/Isola",
			"title": "Primavera Izola",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Prešernova cesta 55 , 6310 Izola/Isola",
			"title": "Primavera Izola-DOSTAVA",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Preradovičeva ulica 33, 2000 Maribor",
			"title": "Prometna šola Maribor",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Slovenska 9, 2000 Maribor",
			"title": "Q Slovenska",
			"cenaDoplacila": "4,27 EUR",
			"cenaObroka": "6,90 EUR"
		},
		{
			"address": "Gorkega ulica 45, 2000 Maribor",
			"title": "Q TABOR",
			"cenaDoplacila": "4,27 EUR",
			"cenaObroka": "6,90 EUR"
		},
		{
			"address": "Cankarjeva cesta 6, 1000 Ljubljana",
			"title": "RESDOBR",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Osojnikova 12, 2250 Ptuj",
			"title": "Restavraciaj Amfora - DOSTAVA",
			"cenaDoplacila": "3,65 EUR",
			"cenaObroka": "6,28 EUR"
		},
		{
			"address": "Poštna ulica 8, 2000 Maribor",
			"title": "Restavracija  \"Baščaršija\"",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Osojnikova 12, 2250 Ptuj",
			"title": "Restavracija  Amfora",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Dunajska 5, 1000 Ljubljana",
			"title": "Restavracija 123 Delo",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Litostrojska 54, 1000 Ljubljana",
			"title": "Restavracija 123 DSU",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Večna pot 113, 1000 Ljubljana",
			"title": "Restavracija 123 FRI in FKKT",
			"cenaDoplacila": "2,20 EUR",
			"cenaObroka": "4,83 EUR"
		},
		{
			"address": "Cesta Ljubljanske brigade 33, 1000 Ljubljana",
			"title": "Restavracija 123 MC Ljubjana",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Ulica Eve Lovše 1, 2000 Maribor",
			"title": "Restavracija 123 MC Maribor",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Industrijska cesta 6, 5000 Nova Gorica",
			"title": "Restavracija 123 MC Nova Gorica",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Verovškova ulica 55a, 1000 Ljubljana",
			"title": "Restavracija 123 Mega center 2",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Dunajska cesta 107, 1000 Ljubljana",
			"title": "Restavracija 123 Mercator Dunajska",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Kardeljeva ploščad 16, 1000 Ljubljana",
			"title": "Restavracija 123 Pedagoška",
			"cenaDoplacila": "2,36 EUR",
			"cenaObroka": "4,99 EUR"
		},
		{
			"address": "Vojkovo nabrežje 32, 6000 Koper/Capodistria",
			"title": "Restavracija 123 Pristan",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Trg republike 1, 1000 Ljubljana",
			"title": "Restavracija 2000",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Nazorjeva ulica 8, 1000 Ljubljana",
			"title": "Restavracija Allegria",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Jurčičeva ulica 7, 2000 Maribor",
			"title": "Restavracija Ancora",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Dunajska cesta 22, 1000 Ljubljana",
			"title": "Restavracija Brejk",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Šmartinska cesta 152g, 1000 Ljubljana",
			"title": "Restavracija Burger King",
			"cenaDoplacila": "1,37 EUR",
			"cenaObroka": "4,00 EUR"
		},
		{
			"address": "Trg Matije Gubca 3, 8270 Krško",
			"title": "Restavracija City",
			"cenaDoplacila": "3,77 EUR",
			"cenaObroka": "6,40 EUR"
		},
		{
			"address": "Titov trg 3, 3320 Velenje",
			"title": "Restavracija DK",
			"cenaDoplacila": "2,47 EUR",
			"cenaObroka": "5,10 EUR"
		},
		{
			"address": "Petkovškovo nabrežje 19, 1000 Ljubljana",
			"title": "Restavracija Fany &amp; Mary",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Slovenska cesta 51, 1000 Ljubljana",
			"title": "Restavracija Fresco",
			"cenaDoplacila": "4,00 EUR",
			"cenaObroka": "6,63 EUR"
		},
		{
			"address": "Mala ulica 5, 1000 Ljubljana",
			"title": "Restavracija GIG",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Kersnikova 6, 1000 Ljubljana",
			"title": "Restavracija in kavarna FOODIE",
			"cenaDoplacila": "3,58 EUR",
			"cenaObroka": "6,21 EUR"
		},
		{
			"address": "Trg zmage 8, 9000 Murska Sobota",
			"title": "Restavracija in pivnica Zvezda",
			"cenaDoplacila": "2,67 EUR",
			"cenaObroka": "5,30 EUR"
		},
		{
			"address": "Mariborska 100, 3000 Celje",
			"title": "Restavracija Interspar Celje",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Šmartinska 152g, 1000 Ljubljana",
			"title": "Restavracija Interspar Citypark",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Ankaranska cesta 3A, 6000 Koper/Capodistria",
			"title": "Restavracija Interspar Koper",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Cesta 1.maja 77, 4000 Kranj",
			"title": "Restavracija Interspar Kranj",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Pobreška 18, 2000 Maribor",
			"title": "Restavracija Interspar Maribor Europark",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "23.4.2019 - 31.7.2019",
			"title": "Restavracija Interspar Maribor Qlandia",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Cesta Proletarskih brigad 100, 2000 Maribor",
			"title": "Restavracija Interspar Murska Sobota",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Nemčavci 1d, 9000 Murska Sobota",
			"title": "Restavracija Interspar Nova Gorica",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Cesta 25. junija 1a, 5000 Nova Gorica",
			"title": "Restavracija Interspar Vič",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Jamova 105, 1000 Ljubljana",
			"title": "Restavracija Kavarna pizzeria Hombre",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Gerbičeva ulica 61, 1000 Ljubljana",
			"title": "Restavracija Kitajska palača",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Focheva ulica 41, 2000 Maribor",
			"title": "Restavracija Kitajska zvezda",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Litijska 28, 1000 Ljubljana",
			"title": "Restavracija Kitajski vrt",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Dravska cesta 7 , 2250 Ptuj",
			"title": "Restavracija klub Cankarjevega doma",
			"cenaDoplacila": "3,97 EUR",
			"cenaObroka": "6,60 EUR"
		},
		{
			"address": "Prešernova cesta 10, 1000 Ljubljana",
			"title": "Restavracija Kolodvorska",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Cesta talcev 37a, 3320 Velenje",
			"title": "Restavracija Kompliment",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Kidričeva cesta 75, 4220 Škofja Loka",
			"title": "Restavracija KOTNIKOVA",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Kotnikova 28, 1000 Ljubljana",
			"title": "Restavracija Mango",
			"cenaDoplacila": "3,60 EUR",
			"cenaObroka": "6,23 EUR"
		},
		{
			"address": "Turnerjeva ulica 17, 2000 Maribor",
			"title": "Restavracija McDonalds - Citycenter",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Mariborska cesta 100, 3000 Celje",
			"title": "Restavracija McDonalds - Ptuj",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Ormoška cesta 3, 2250 Ptuj",
			"title": "Restavracija McDonald's Čopova",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Čopova 14, 1000 Ljubljana",
			"title": "Restavracija McDonald's Kolosej",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Šmartinska 152, 1000 Ljubljana",
			"title": "Restavracija McDonald's Novo mesto",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Ljubljanska 24, 8000 Novo mesto",
			"title": "Restavracija McDonald's Rudnik",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Premrlova ulica 12, 1000 Ljubljana",
			"title": "Restavracija McDonald's -Železniška",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Trg osvobodilne fronte 6, 1000 Ljubljana",
			"title": "Restavracija McDonald's Žito",
			"cenaDoplacila": "2,07 EUR",
			"cenaObroka": "4,70 EUR"
		},
		{
			"address": "Šmartinska 154, 1000 Ljubljana",
			"title": "Restavracija Modri kvadrat",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Davčna ulica 1, 1000 Ljubljana",
			"title": "Restavracija Mozart",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Stegne 7, 1000 Ljubljana",
			"title": "Restavracija Mr. FALAFEL",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Gosposka ulica 30, 2000 Maribor",
			"title": "Restavracija ORIENT",
			"cenaDoplacila": "3,10 EUR",
			"cenaObroka": "5,73 EUR"
		},
		{
			"address": "Taborska ulica 12, 2000 Maribor",
			"title": "Restavracija Peking",
			"cenaDoplacila": "2,00 EUR",
			"cenaObroka": "4,63 EUR"
		},
		{
			"address": "Gregorčičeva ulica 19, 2000 Maribor",
			"title": "Restavracija Piano",
			"cenaDoplacila": "3,26 EUR",
			"cenaObroka": "5,89 EUR"
		},
		{
			"address": "Koroška cesta 160, 2000 Maribor",
			"title": "Restavracija Plečnikov hram",
			"cenaDoplacila": "3,60 EUR",
			"cenaObroka": "6,23 EUR"
		},
		{
			"address": "Trg francoske revolucije 2, 1000 Ljubljana",
			"title": "Restavracija Prestige catering, GZS",
			"cenaDoplacila": "3,17 EUR",
			"cenaObroka": "5,80 EUR"
		},
		{
			"address": "Dimičeva ulica 13, 1000 Ljubljana",
			"title": "Restavracija Rdeče jabolko",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Tržaška 116, 1000 Ljubljana",
			"title": "Restavracija Rutar Ljubljana",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Premrlova ulica 14, 1000 Ljubljana",
			"title": "Restavracija Rutar Maribor",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Tržaška cesta 67, 2000 Maribor",
			"title": "Restavracija sarajevskih jedi Valter - Škofja Loka",
			"cenaDoplacila": "2,30 EUR",
			"cenaObroka": "4,93 EUR"
		},
		{
			"address": "Kidričeva cesta 8c, 4220 Škofja Loka",
			"title": "Restavracija sarajevskih jedi Valter Jesenice",
			"cenaDoplacila": "2,30 EUR",
			"cenaObroka": "4,93 EUR"
		},
		{
			"address": "Cesta Maršala Tita 106, 4270 Jesenice",
			"title": "Restavracija Sarajevskih jedi Valter Kranj",
			"cenaDoplacila": "2,30 EUR",
			"cenaObroka": "4,93 EUR"
		},
		{
			"address": "Cesta 1.maja 1a, 4000 Kranj",
			"title": "Restavracija sarajevskih jedi Valter Ljubljana",
			"cenaDoplacila": "2,30 EUR",
			"cenaObroka": "4,93 EUR"
		},
		{
			"address": "Šmartinska cesta 3, 1000 Ljubljana",
			"title": "Restavracija sarajevskij jedi Valter Radovljica",
			"cenaDoplacila": "2,30 EUR",
			"cenaObroka": "4,93 EUR"
		},
		{
			"address": "Cankarjeva 66, 4240 Radovljica",
			"title": "Restavracija Shaolin",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Hacquetova 5, 1000 Ljubljana",
			"title": "Restavracija Skriti kot",
			"cenaDoplacila": "3,90 EUR",
			"cenaObroka": "6,53 EUR"
		},
		{
			"address": "Ajdovščina 4, 1000 Ljubljana",
			"title": "Restavracija Slorest Vičava",
			"cenaDoplacila": "1,86 EUR",
			"cenaObroka": "4,49 EUR"
		},
		{
			"address": "Vičava 1, 2250 Ptuj",
			"title": "Restavracija Splošne bolnišnice Novo mesto",
			"cenaDoplacila": "1,87 EUR",
			"cenaObroka": "4,50 EUR"
		},
		{
			"address": "Šmihelska cesta 1, 8000 Novo mesto",
			"title": "Restavracija Šmartinska",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Šmartinska cesta 132, 1000 Ljubljana",
			"title": "Restavracija šolski center Aškerčeva",
			"cenaDoplacila": "1,87 EUR",
			"cenaObroka": "4,50 EUR"
		},
		{
			"address": "Aškerčeva 1, 1000 Ljubljana",
			"title": "Restavracija Tartuf \"Center\"",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Mali trg 3a, 1000 Ljubljana",
			"title": "Restavracija Tomi",
			"cenaDoplacila": "2,97 EUR",
			"cenaObroka": "5,60 EUR"
		},
		{
			"address": "Iztokova ulica 22, 2000 Maribor",
			"title": "Restavracija TR3",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Trg repubilke 3, 1000 Ljubljana",
			"title": "Restavracija Tuš Celje (Planet Tuš)",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Mariborska cesta 128, 3000 Celje",
			"title": "Restavracija Tuš Kranj",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Cesta Jake Platiše 18, 4000 Kranj",
			"title": "Restavracija Zlata sreča",
			"cenaDoplacila": "2,50 EUR",
			"cenaObroka": "5,13 EUR"
		},
		{
			"address": "Metelkova ulica 29, 2000 Maribor",
			"title": "Restavracija,picerija, špageterija ADRENALINA",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Pesje 30, 8270 Krško",
			"title": "Roza slon",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Trg OF 14, 1000 Ljubljana",
			"title": "Roza slon Bežigrad",
			"cenaDoplacila": "2,90 EUR",
			"cenaObroka": "5,53 EUR"
		},
		{
			"address": "Dunajska 115, 1000 Ljubljana",
			"title": "Running sushi &amp; wok",
			"cenaDoplacila": "3,87 EUR",
			"cenaObroka": "6,50 EUR"
		},
		{
			"address": "Jurčkova cesta 223, 1000 Ljubljana",
			"title": "SALAMON - dostava",
			"cenaDoplacila": "4,17 EUR",
			"cenaObroka": "6,80 EUR"
		},
		{
			"address": "Dolenjska cesta 103, 1000 Ljubljana",
			"title": "Samopostrežna restavracija Dobra hiša",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Stegne 11a, 1000 Ljubljana",
			"title": "Samopostrežna restavracija Stolpič",
			"cenaDoplacila": "2,87 EUR",
			"cenaObroka": "5,50 EUR"
		},
		{
			"address": "Mariborska cesta 7, 3000 Celje",
			"title": "Skuhna",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Trubarjeva 56, 1000 Ljubljana",
			"title": "Slaščičarna in kavarna Leonard",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Trg mladinskih delovnih brigad 12, 1000 Ljubljana",
			"title": "Slaščičarna in picerija AJDOVŠČINA",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Goriška cesta 11, 5270 Ajdovščina",
			"title": "Slaščičarna in picerija Ajdovščina - HALO ERIK-DOSTAVA",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Goriška cesta 11, 5270 Ajdovščina",
			"title": "Spar partner Nama",
			"cenaDoplacila": "2,37 EUR",
			"cenaObroka": "5,00 EUR"
		},
		{
			"address": "Tomšičeva 2, 1000 Ljubljana",
			"title": "Splošna bolnišnica Izola",
			"cenaDoplacila": "2,37 EUR",
			"cenaObroka": "5,00 EUR"
		},
		{
			"address": "Polje 40, 6310 Izola/Isola",
			"title": "Srednja šola Izola - Scuola media Isola",
			"cenaDoplacila": "2,00 EUR",
			"cenaObroka": "4,63 EUR"
		},
		{
			"address": "Prekomorskih brigad 7, 6310 Izola/Isola",
			"title": "Stara krška pizzerija",
			"cenaDoplacila": "3,37 EUR",
			"cenaObroka": "6,00 EUR"
		},
		{
			"address": "Cesta krških žrtev 59, 8270 Krško",
			"title": "Stara krška pizzerija - dostava",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Cesta krških žrtev 59, 8270 Krško",
			"title": "Subway",
			"cenaDoplacila": "1,50 EUR",
			"cenaObroka": "4,13 EUR"
		},
		{
			"address": "Dunajska cesta 115a, 1000 Ljubljana",
			"title": "Subway",
			"cenaDoplacila": "1,50 EUR",
			"cenaObroka": "4,13 EUR"
		},
		{
			"address": "Slovenska cesta 1, 1000 Ljubljana",
			"title": "Subway BTC",
			"cenaDoplacila": "1,50 EUR",
			"cenaObroka": "4,13 EUR"
		},
		{
			"address": "Šmartinska cesta 152, 1000 Ljubljana",
			"title": "Šeherezada",
			"cenaDoplacila": "1,37 EUR",
			"cenaObroka": "4,00 EUR"
		},
		{
			"address": "Trubarjeva cesta 31, 1000 Ljubljana",
			"title": "Šolski center Šentjur",
			"cenaDoplacila": "0,87 EUR",
			"cenaObroka": "3,50 EUR"
		},
		{
			"address": "Cesta na Kmetijsko šolo 9, 3230 Šentjur",
			"title": "Študentski dom Ljubljana - Restavracija",
			"cenaDoplacila": "3,10 EUR",
			"cenaObroka": "5,73 EUR"
		},
		{
			"address": "Svetčeva ulica 9, 1000 Ljubljana",
			"title": "Tajska restavracija Bangkok Street",
			"cenaDoplacila": "3,67 EUR",
			"cenaObroka": "6,30 EUR"
		},
		{
			"address": "Šmartinska 152g, 1000 Ljubljana",
			"title": "Takos",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Mesarski prehod 3, 2000 Maribor",
			"title": "Takos dos",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Pobreška cesta 18, 2000 Maribor",
			"title": "TANDORI CAFE",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Ilirska ulica 8, 1000 Ljubljana",
			"title": "TANDORI CAFE - DOSTAVA",
			"cenaDoplacila": "3,70 EUR",
			"cenaObroka": "6,33 EUR"
		},
		{
			"address": "Ilirska ulica 8, 1000 Ljubljana",
			"title": "Tasty snacks",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Cafova ulica 4, 2000 Maribor",
			"title": "Taverna Sicilia",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Ulica Heroja Nandeta 31, 2000 Maribor",
			"title": "Taverna Sicilia - DOSTAVA",
			"cenaDoplacila": "3,57 EUR",
			"cenaObroka": "6,20 EUR"
		},
		{
			"address": "Ulica Heroja Nandeta 31, 2000 Maribor",
			"title": "Trattoria Padrino",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Žitna ulica 12, 2000 Maribor",
			"title": "Trattoria Portofino",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Tržaška cesta 65, 2000 Maribor",
			"title": "Tuš restavracija Maribor Studenci (Planet Tuš)",
			"cenaDoplacila": "2,99 EUR",
			"cenaObroka": "5,62 EUR"
		},
		{
			"address": "Na Poljanah 18, 2000 Maribor",
			"title": "UFO",
			"cenaDoplacila": "2,98 EUR",
			"cenaObroka": "5,61 EUR"
		},
		{
			"address": "Svetčeva ulica 14, 1000 Ljubljana",
			"title": "Un momento-pizza time",
			"cenaDoplacila": "0,80 EUR",
			"cenaObroka": "3,43 EUR"
		},
		{
			"address": "Pobreška cesta 18, 2000 Maribor",
			"title": "UTRIP - bar, bistro in kavarna",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Šmartinska cesta 3, 1000 Ljubljana",
			"title": "V parku",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Rimska cesta 13, 1000 Ljubljana",
			"title": "Vegetarijanska okrepčevalnica HOLYFOOD",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Igriška 5, 1000 Ljubljana",
			"title": "Vegetarijanska restavracija Radha Govinda",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Žibretova 23, 1000 Ljubljana",
			"title": "Village snacks",
			"cenaDoplacila": "0,50 EUR",
			"cenaObroka": "3,13 EUR"
		},
		{
			"address": "Turnerjeva ulica 17, 2000 Maribor",
			"title": "VOLTA",
			"cenaDoplacila": "3,00 EUR",
			"cenaObroka": "5,63 EUR"
		},
		{
			"address": "Tržaška cesta 18, 1000 Ljubljana",
			"title": "Vozi Miško (MC Šiška)",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Cesta Ljubljanske brigade 33, 1000 Ljubljana",
			"title": "Wok mix okrepčevalnica",
			"cenaDoplacila": "2,70 EUR",
			"cenaObroka": "5,33 EUR"
		},
		{
			"address": "Vetrinjska ulica 15, 2000 Maribor",
			"title": "XIAO FENG",
			"cenaDoplacila": "3,30 EUR",
			"cenaObroka": "5,93 EUR"
		},
		{
			"address": "Bazoviška ulica 6, 5000 Nova Gorica",
			"title": "Zbornica bar in žar",
			"cenaDoplacila": "4,37 EUR",
			"cenaObroka": "7,00 EUR"
		},
		{
			"address": "Rimska cesta 13, 1000 Ljubljana",
			"title": "Zlati krožnik",
			"cenaDoplacila": "3,50 EUR",
			"cenaObroka": "6,13 EUR"
		},
		{
			"address": "Ljubljanska cesta 89, 1230 Domžale",
			"title": "Znanstvena kavarna Mafija",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Jadranska ulica 21, 1000 Ljubljana",
			"title": "Žito Leon Štukelj Maribor",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Trg Leona Štuklja 1, 2000 Maribor",
			"title": "ŽITO Ljublana železniška (kolodvor)",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Trg OF 7, 1000 Ljubljana",
			"title": "ŽITO Ljubljana Bavarski dvor",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		},
		{
			"address": "Slovenska cesta 58, 1000 Ljubljana",
			"title": "ŽITO Ljubljana Vodnik",
			"cenaDoplacila": "0,00 EUR",
			"cenaObroka": "2,63 EUR"
		}
	];

	rek(db, client, arr);

});


function sendRequest(arr, index, cb) {
	var url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + encodeURIComponent(arr[index].title) + "&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry,place_id&key=AIzaSyBFpco1Si1VVnT93GNcoUiYnQroAN1FsmI"
	https.get(url, (resp) => {
		let data = '';

		resp.on('data', (chunk) => {
			data += chunk;
		});

		resp.on('end', () => {
			cb(data);
		});

	}).on("error", (err) => {
		console.log("Error: " + err.message);
		cb("error");
	});
}

function sendRequest2(arr, index, cb) {
	var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURIComponent(arr[index].address) + "&key=AIzaSyBFpco1Si1VVnT93GNcoUiYnQroAN1FsmI"
	https.get(url, (resp) => {
		let data = '';

		resp.on('data', (chunk) => {
			data += chunk;
		});

		resp.on('end', () => {
			cb(data);
		});

	}).on("error", (err) => {
		console.log("Error: " + err.message);
		cb("error");
	});
}

function rek(db, client, arr) {
	if (index == arr.length) {
		client.close();
		return;
	}


	sendRequest(arr, index, function(data) {

		var obj = JSON.parse(data);

		if (obj.status == "OK") {

			arr[index].lat = obj.candidates[0].geometry.location.lat;
			arr[index].lon = obj.candidates[0].geometry.location.lng;
			arr[index].rating = obj.candidates[0].rating;
			console.log(arr[index].rating);

			db.collection('restaurants').insertOne(arr[index], function(err, r) {
				if (err) {
					throw err;
				}
				index++;
				rek(db, client, arr);
				console.log("document with index " + index + " has been successfuly inserted!");
			});
		} else {

			sendRequest2(arr, index, function(data) {
				var obj = JSON.parse(data);
				if (obj.status == "OK") {
					arr[index].lat = obj.results[0].geometry.location.lat;
					arr[index].lon = obj.results[0].geometry.location.lng;

					db.collection('restaurants').insertOne(arr[index], function(err, r) {
						if (err) {
							throw err;
						}
						index++;
						rek(db, client, arr);
						console.log("document with index " + index + " has been inserted but with sendrequest2 lat and lng params!");
					});

				}
			});

		}

	});

}