function openModalTodo(){
	$("#modal_todo").modal("toggle");
}

function openModalUrnik(){
	$('#modal_urnik').modal('toggle');
}

function addNewNote(){
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			console.log(res);
			addNewNoteToHtml(res);
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("PUT", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/note", true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	
	var textNew = $("#input_todo_hehe");
	
	
	if(textNew.val().length == 0){
		return;
	}
	
	var data = {
		text: textNew.val()
	};
	
	xhttp.send(JSON.stringify(data));
}

function addNewNoteToHtml(obj){
	openModalTodo();
	var nodes = document.getElementById("all_note_preview").childNodes;
	var string = "";
	var randomCol = whichCol(nodes);
	string += "<div class='card' style='background-color:rgb(222,222,222);margin-top:5px;margin-right:5px;margin-bottom:10px;margin-left:5px;'>";
	string += "<div class='card-body d-flex flex-column'>";
	string += "<textarea data-id='" + obj["_id"] + "' style='background-color:rgba(255,255,255,0);color:rgb(0,0,0);height:250px;font-size:14px;' onfocusout='updateMyTodo(this)'>" + obj["text"]+ "</textarea>";
	string += "<a data-id='" + obj["_id"] + "' class='card-link float-right align-self-end' style='color:rgb(186,0,0);font-size:12px;cursor:pointer;' onclick='deleteTodoNote(this)'><strong>Delete</strong></a></div></div>";	

	$(nodes[randomCol]).append(string);
}

function whichCol(nodes){
	var col1 = nodes[0].childNodes.length;
	var col2 = nodes[1].childNodes.length;
	var col3 = nodes[2].childNodes.length;
	
	return (col1 + col2 + col3)%3;
}

/* DELETE TODO NOTE */
function deleteTodoNote(e){
	var idTodo = e.getAttribute("data-id");
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			console.log(res);
			removeTodoFromHTML(e);
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("DELETE", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/note/" + idTodo, true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhttp.send();
}

function removeTodoFromHTML(e){
	e.parentNode.parentNode.remove();
}

function updateMyTodo(e){
	var idTodo = e.getAttribute("data-id");
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			console.log(res);
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("PUT", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/note/" + idTodo, true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	var textNew = $(e);
	
	if(textNew.val().length == 0){
		return;
	}

	var data = {
		text: textNew.val()
	};
	
	xhttp.send(JSON.stringify(data));
}

var savedIdForModalUrnik = "";
var col = "";
var row = "";
var dateArraySlot = [
	new Date('December 11, 1995 12:00:00'), // Mon
	new Date('December 12, 1995 12:00:00'), // Tue
	new Date('December 13, 1995 12:00:00'), // Wed
	new Date('December 14, 1995 12:00:00'), // Thu
	new Date('December 15, 1995 12:00:00'), // Fri
	new Date('December 16, 1995 12:00:00'), // Sat
	new Date('December 17, 1995 12:00:00'), // Sun
];


/* Modify urnik slot */
function urnikModify(e){
	col = e.getAttribute("data-col");
	row = e.getAttribute("data-row");
	var urnikIdSlot = e.getAttribute("data-id");
	var textSlot = e.innerHTML;
	savedIdForModalUrnik = "";
	
	if(urnikIdSlot && urnikIdSlot.length != ""){
		savedIdForModalUrnik = urnikIdSlot;
		$("#delete_timetable_slot").css("display","block");
		$("#button_timetable_slot").html("Save");
	}else{
		$("#delete_timetable_slot").css("display","none");
		$("#button_timetable_slot").html("Add");
	}

	$('#input_course_title').val(textSlot);
	openModalUrnik();
}

function updateOrAddSlot(e){
	var xhttp = new XMLHttpRequest();
	console.log("kaj1===");
	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			console.log(res);
			var selectedSlot = $('[data-row="' + row + '"][data-col="' + col + '"]');
			selectedSlot.html(res.title);
			selectedSlot.attr("data-id", res["_id"]);
			console.log(selectedSlot);
			openModalUrnik();
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	var idSlot = savedIdForModalUrnik;
	
	if(!idSlot || idSlot.length == 0){
		xhttp.open("PUT", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/slot", true);
	}else{
		xhttp.open("PUT", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/slot/" + idSlot, true);
	}
	
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	var textNew = $('#input_course_title');
	
	if(textNew.val().length == 0){
		return;
	}
	
	var datum = dateArraySlot[col];
	datum.setHours(row);
	
	var data = {
		title: textNew.val(),
		date: datum
	};
	
	xhttp.send(JSON.stringify(data));
}

function deleteSlot(e){
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			console.log(res);
			var selectedSlot = $('[data-row="' + row + '"][data-col="' + col + '"]');
			selectedSlot.removeAttr("data-id");
			selectedSlot.html("");
			openModalUrnik();
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	var idSlot = savedIdForModalUrnik;
	
	xhttp.open("DELETE", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/slot/" + idSlot, true);

	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	xhttp.send();
}

var dateHashMap = {
	"0":6, // Sun
	"1":0, // Mon
	"2":1, // Tue
	"3":2, // Wed
	"4":3, // Thu
	"5":4, // Fri
	"6":5  // Sat
	};

var monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"];
var currentDateObj = new Date();
currentDateObj.setDate(14);	
var lastIdCalendarEvent = "";
	
$(document).ready(function() {
	getTimetable();
	callFromDBCalendar();
});

function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}

function callFromDBCalendar(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			fillCalendar(currentDateObj, res.calendar);
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("GET", location.protocol + "//" + location.host + "/api/currentUser", true);

	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	xhttp.send();
}


function fillCalendar(date, userData){
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	var start = dateHashMap[firstDay.getDay()];
	
	var monthLength = daysInMonth(date.getMonth()+1, date.getYear());
	$("#monthNameOriginal").html(monthArray[date.getMonth()]);
	
	var j = 1;
	var table = $("#koledar_straightAs td");
	
	for(var i = 0; i < 6*7;i++){
		if(i < start){
			$(table[i]).html("&nbsp;");
		}else if(i > monthLength + start - 1){
			$(table[i]).html("&nbsp;");
		}else{
			$(table[i]).html(j);
			addToCalendarCell(userData, $(table[i]), j);
			j++;
		}
	}
}

function addToCalendarCell(obj, cell, day){
	for(var i = 0; i < obj.length; i++){
		var cDate = new Date(obj[i].startTime);
		
		if(day == cDate.getDate() && currentDateObj.getMonth() == cDate.getMonth() && currentDateObj.getYear() == cDate.getYear()){
			var string = "<i data-id='" + obj[i]["_id"] + "' class='icon ion-android-bookmark' style='font-size:20px;display:inline-block;cursor:pointer;' onclick='editCalendarEdit(this)'></i>";
			cell.append(string);
		}
	}
}

function genNextMonth(){
	currentDateObj.setMonth(currentDateObj.getMonth() + 1);
	callFromDBCalendar();
}

function genPrevMonth(){
	currentDateObj.setMonth(currentDateObj.getMonth() - 1);
	callFromDBCalendar();
}

function getTimetable(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			
			for(var i = 0; i < res.length; i++){
				var datumSlot = new Date(res[i].date);
				var day = dateHashMap[datumSlot.getDay()]; // 6 = Saturday, 0 = Sunday
				var h = datumSlot.getHours();
			
				var selectedSlot = $('[data-row="' + h + '"][data-col="' + day + '"]');
				selectedSlot.attr("data-id", res[i]["_id"]);
				selectedSlot.html(res[i].title);
			}
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("GET", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/slot", true);

	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	xhttp.send();
}

function editCalendar(){
	// odpre modal v načinu za dodajo novega dogodka
	lastIdCalendarEvent = "";
	clearModalUrnik();
	$("#delete_calendar_button").css("display", "none");
	$("#save_calendar_button").css("display", "none");
	$("#add_calendar_button").css("display", "block");
	$("#modal_koledar").modal("toggle");
}

function addPaddingTime(t){
	if(t < 10)
		return "0"+t;
	return ""+t;
}

function editCalendarEdit(e){
	// odpre modal v načinu za dodajo novega dogodka
	lastIdCalendarEvent = e.getAttribute("data-id");
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText).data;
			var startDate = new Date(res.startTime);
			var endDate = new Date(res.endTime);		
			
			var dayS = ("0" + startDate.getDate()).slice(-2);
			var monthS = ("0" + (startDate.getMonth() + 1)).slice(-2);
			var todayS = startDate.getFullYear()+"-"+(monthS)+"-"+(dayS) ;
					
			var dayE = ("0" + endDate.getDate()).slice(-2);
			var monthE = ("0" + (endDate.getMonth() + 1)).slice(-2);
			var todayE = endDate.getFullYear()+"-"+(monthE)+"-"+(dayE);
			
			var hS = addPaddingTime(startDate.getHours());
			var hE = addPaddingTime(endDate.getHours());
			var mS = addPaddingTime(startDate.getMinutes());
			var mE = addPaddingTime(endDate.getMinutes());
			
			$("#input_calendar_title").val(res.title);
			$("#input_calendar_start_date").val(todayS);
			$("#input_calendar_start_date_time").val(hS+":"+mS);
			$("#input_calendar_end_date").val(todayE);
			$("#input_calendar_end_date_time").val(hE+":"+mE);
			$("#input_calendar_description").val(res.description);
			
			$("#delete_calendar_button").css("display", "block");
			$("#save_calendar_button").css("display", "block");
			$("#add_calendar_button").css("display", "none");
			$("#modal_koledar").modal("toggle");
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("GET", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/event/" + lastIdCalendarEvent, true);

	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	xhttp.send();
}

function addCalendarEvent(){
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			lastIdCalendarEvent = "";
			$("#modal_koledar").modal("toggle");
			fillCalendar(currentDateObj, res);
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("PUT", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/event", true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	
	var title = $("#input_calendar_title").val();
	var startDate = $("#input_calendar_start_date").val();
	var startTime = $("#input_calendar_start_date_time").val().split(":");
	var endDate = $("#input_calendar_end_date").val();
	var endTime = $("#input_calendar_end_date_time").val().split(":");
	var description = $("#input_calendar_description").val();
	
	var modifyStartDate = new Date(startDate);
	var modifyEndDate = new Date(endDate);
	
	modifyStartDate.setHours(startTime[0]);
	modifyStartDate.setMinutes(startTime[1]);
	
	modifyEndDate.setHours(endTime[0]); // mogoce je lahk problem REEEEEEEE
	modifyEndDate.setMinutes(endTime[1]);
	
	
	console.log(title);
	console.log(modifyStartDate);
	console.log(modifyEndDate);
	console.log(description);
	
	var data = {
		title: title,
		startTime: modifyStartDate,
		endTime: modifyEndDate,
		description: description,
	};
	
	xhttp.send(JSON.stringify(data));
}

function saveCalendarEvent(){
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			lastIdCalendarEvent = "";
			$("#modal_koledar").modal("toggle");
			fillCalendar(currentDateObj, res);
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("PUT", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/event/" + lastIdCalendarEvent, true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	
	var title = $("#input_calendar_title").val();
	var startDate = $("#input_calendar_start_date").val();
	var startTime = $("#input_calendar_start_date_time").val().split(":");
	var endDate = $("#input_calendar_end_date").val();
	var endTime = $("#input_calendar_end_date_time").val().split(":");
	var description = $("#input_calendar_description").val();
	
	var modifyStartDate = new Date(startDate);
	var modifyEndDate = new Date(endDate);
	
	modifyStartDate.setHours(startTime[0]);
	modifyStartDate.setMinutes(startTime[1]);
	
	modifyEndDate.setHours(endTime[0]); // mogoce je lahk problem REEEEEEEE
	modifyEndDate.setMinutes(endTime[1]);
	
	
	console.log(title);
	console.log(modifyStartDate);
	console.log(modifyEndDate);
	console.log(description);
	
	var data = {
		title: title,
		startTime: modifyStartDate,
		endTime: modifyEndDate,
		description: description,
	};
	
	xhttp.send(JSON.stringify(data));
}

function deleteCalendarEvent(){
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.responseText);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			$("[data-id='" + lastIdCalendarEvent + "']").remove();
			lastIdCalendarEvent = "";
			$("#modal_koledar").modal("toggle");
		}
	}

	var token = getCookie("token");
	
	if(!token){
		return;
	}
	
	var decoded = jwt_decode(token);
	
	xhttp.open("DELETE", location.protocol + "//" + location.host + "/api/users/" + decoded.id + "/event/" + lastIdCalendarEvent, true);

	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	xhttp.send();
}

function clearModalUrnik(){
	$("#input_calendar_title").val("");
	$("#input_calendar_start_date").val("");
	$("#input_calendar_start_date_time").val("");
	$("#input_calendar_end_date").val("");
	$("#input_calendar_end_date_time").val("");
	$("#input_calendar_description").val("");
}