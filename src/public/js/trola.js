window.onload = function() {
	console.log("loaded!");
}


function sendRequest() {
	var stationName = document.getElementById("stationName").value.trim();
	var routeNumber = document.getElementById("routeNumber").value.trim();

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var obj = JSON.parse(xhttp.responseText);
			if (obj.message == "OK") {
				var arr = obj.data.stations;
				generateTable(arr);
			}
			console.log(xhttp.responseText);
		}
	}



	var url = location.protocol + "//" + location.host + "/api/bus/" + stationName + "/" + routeNumber;
	console.log(url);

	xhttp.open("GET", url, true);
	xhttp.send();

	console.log("send request");

}

function generateTable(arr) {

	document.getElementById("tableBody").innerHTML = "";
	var str = "";
	for (var i = 0; i < arr.length; i++) {
		var sName = arr[i].name;
		var buses = arr[i].buses;

		for (var j = 0; j < buses.length; j++) {

			var dir = buses[j].direction;
			var n = buses[j].number;
			var busArrivals = buses[j].arrivals;

			for (var k = 0; k < busArrivals.length; k++) {
				str += "<tr>"

				str += "<th>" + sName + "</th>";
				str += "<td>" + dir + "</td>"
				str += "<td>" + n + "</td>"
				str += "<td>" + busArrivals[k] + "</td>"
			}
			str += "</tr>"
		}
	}


	document.getElementById("tableBody").innerHTML += str;
}