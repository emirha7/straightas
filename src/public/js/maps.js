var map = null;

var ljubljanaLat = 46.056946;
var ljubljanaLon = 14.505751;

var userLat = ljubljanaLat;
var userLon = ljubljanaLon;

var markers = [];

var b = false;

var allRestaurants = [];

function mapInit() {
	var mapProp = {
		center: new google.maps.LatLng(ljubljanaLat, ljubljanaLon),
		zoom: 13
	};

	map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}

function sendRequest() {
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var jsonArray = JSON.parse(this.responseText);
			allRestaurants = jsonArray;
			console.log(allRestaurants);
			console.log(b);
			if (b) {
				calculatePositionsGivenArray(allRestaurants);
			}
		}
	}

	xhttp.open("GET", location.protocol + "//" + location.host + "/api/maps", true);
	xhttp.send();

}

function calculatePositionsGivenArray(jsonArray) {
	if (jsonArray.data) {
		console.log("kalkuliram pozicijo!")
		for (var i = 0; i < jsonArray.data.length; i++) {
			setMarker(jsonArray.data[i], jsonArray.data[i].lat, jsonArray.data[i].lon);
		}
	}
}

var rad = function(x) {
	return x * Math.PI / 180;
};

function degreesToRadians(degrees) {
	return degrees * Math.PI / 180;
}

function getDistance(lat1, lon1, lat2, lon2) {
	var earthRadiusKm = 6371;

	var dLat = degreesToRadians(lat2 - lat1);
	var dLon = degreesToRadians(lon2 - lon1);

	lat1 = degreesToRadians(lat1);
	lat2 = degreesToRadians(lat2);

	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	return earthRadiusKm * c;
}

function formatDistance(x) {
	return parseFloat(Math.round(x * 100) / 100).toFixed(2);
}

function setMarker(obj, lat, lng) {

	var calculateDistance = getDistance(userLat, userLon, lat, lng);

	if (calculateDistance < 200) {

		var stars = "";
		console.log(Math.floor(obj.rating));
		for(var i = 0; i < Math.floor(obj.rating); i++){
			stars+="<i style='color: #f5b301;' class='fas fa-star'></i>";
		}

		if(Math.floor(obj.rating)!=Math.ceil(obj.rating)){
			stars+="<i style='color: #f5b301;' class='fas fa-star-half'></i>";
		}

		var contentString = "<div id='content'>" +
			"<div id='siteNotice'>" +
			"</div>" +
			"<h1 id='firstHeading' class='firstHeading'>" + obj.title + "</h1>" +
			"<div id='bodyContent'>" +
			"<p><b>Cena obroka</b> " + obj.cenaObroka + "" +
			"<p><b>Cena doplacila</b> " + obj.cenaDoplacila + "" +
			"<p><b>Oddaljenost:</b> " + formatDistance(calculateDistance) + " km <p>" +
			"<p>"+stars+"</p>"+
			"</div>" +
			"</div>";

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker1 = new google.maps.Marker({
			position: new google.maps.LatLng(userLat, userLon),
			map: map,
			title: 'You are here!'
		});

		centerMap(userLat, userLon);

		markers.push(marker1);

		var marker2 = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: map,
			icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
		});

		marker2.addListener('click', function() {
			infowindow.open(map, marker2);
		});
	}

}


function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
	} else {
		console.log("Geolocation is not supported by this browser.");
	}
}

function showPosition(position) {
	b = true;
	userLat = position.coords.latitude;
	userLon = position.coords.longitude;

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(userLat, userLon),
		map: map,
		title: 'You are here!'
	});

	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}

	console.log("hahaha!");
	console.log(allRestaurants)
	if (b) {
		calculatePositionsGivenArray(allRestaurants);
	}
	centerMap(userLat, userLon);
}

function centerMap(lat, lon) {
	if (map) {
		map.setCenter(new google.maps.LatLng(lat, lon));
	}
}

function showError(error) {
	if (!b) {
		calculatePositionsGivenArray(allRestaurants);
		b = true;
	}

	centerMap(ljubljanaLat, ljubljanaLon);
}

window.onload = function() {
	getLocation();
	sendRequest();
}