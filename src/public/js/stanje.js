document.addEventListener("DOMContentLoaded", function(event) { 
	checkCookie();
});

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


function checkCookie() {
	console.log("Checking cookie");
	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		console.log(this.status);
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			console.log(res);
			
			if(res.length != 0){
				canShowMessage(res.data[0]);
			}
		}
	}

	xhttp.open("GET", location.protocol + "//" + location.host + "/api/adminMessage", true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	
	xhttp.send();
  
	function canShowMessage(obj){
		var messageIdThatWasShown = getCookie("messageId");
	  
		if(messageIdThatWasShown == ""){
			setCookie("messageId",obj["_id"],1);
			alert("Notification: " + obj.message);
		}else{
			if(messageIdThatWasShown != obj["_id"]){
				setCookie("messageId",obj["_id"],1);
				alert("Notification: " + obj.message);
			}
		}
	}
}

function deleteItem(e){
	console.log(e.getAttribute("data-id"));
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if (this.readyState == 4 && this.status == 200) {
			var res = JSON.parse(this.responseText);
			e.parentNode.parentNode.remove();
			console.log("its me=");
			console.log(res);
		}
		console.log(this.responseText);
	}
	
	xhttp.open("DELETE", location.protocol + "//" + location.host + "/api/publicEvents/"+e.getAttribute("data-id"), true);
	xhttp.setRequestHeader("x-access-token", getCookie("token"), false);
	xhttp.send();
}