var mongoose = require('mongoose');
var crypto = require("crypto");
var config = require("../config/config.js");
var jwt = require('jsonwebtoken');

var eventSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	startTime: {
		type: Date,
		required: true
	},
	endTime: {
		type: Date,
		required: true
	},
	description: String
});

var slotSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	date: {
		type: Date,
		required: true
	}
});

var noteSchema = new mongoose.Schema({
	text: {
		type: String,
		required: true
	}
});

var usersSchema = new mongoose.Schema({
	email: {
		type: mongoose.Schema.Types.String,
		unique: true,
		required: true,
		dropDups: true
	},
	role: {
		type: String,
		required: true
	},
	calendar: [eventSchema],
	timetable: [slotSchema],
	todo: [noteSchema],
	hashedPassword: String,
	randomValue: String
});

var publicEventSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	date: {
		type: Date,
		required: true
	},
	organizer: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	}
});

usersSchema.methods.setPassword = function(password) {
	this.randomValue = crypto.randomBytes(16).toString('hex');
	this.hashedPassword = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
};

usersSchema.methods.checkPassword = function(password) {
	var passwordToCompare = crypto.pbkdf2Sync(password, this.randomValue, 1000, 64, 'sha512').toString('hex');
	return this.hashedPassword == passwordToCompare;
}

usersSchema.methods.generateToken = function() {
	return jwt.sign({
		id: this._id,
		role: this.role
	}, config.secret, {
		expiresIn: 86400
	});
}

var adminNotificationSchema = new mongoose.Schema({
	message: {
		type: String,
		required: true
	}
});

mongoose.model('PublicEvent', publicEventSchema);
mongoose.model('User', usersSchema);
mongoose.model('AdminNotification', adminNotificationSchema);