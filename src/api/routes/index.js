var express = require("express");
var router = express.Router();
var ctrlUsers = require("../controllers/users");
var ctrlMaps = require("../controllers/maps");
var ctrlBus = require("../controllers/bus");
var ctrlNotification = require("../controllers/adminNotification");
var ctrPublicEvents = require('../controllers/publicEvent');
var VerifyToken = require("../controllers/verifyToken");

router.post("/users/updatepassword", VerifyToken.verifyToken, ctrlUsers.changePassword);

router.post("/users/register", ctrlUsers.registerUser);
router.post("/users/login", ctrlUsers.loginUser);
router.get("/currentuser/", VerifyToken.verifyToken, ctrlUsers.getCurrentUser);
router.get("/maps/", ctrlMaps.getAll);

router.get("/bus/:station/:rnumber?", ctrlBus.showBus);

router.post("/adminMessage/", VerifyToken.verifyToken, ctrlNotification.uploadNotification);
router.get("/adminMessage/", VerifyToken.verifyToken, ctrlNotification.getLastNotification);

router.post("/publicEvents/", VerifyToken.verifyToken, ctrPublicEvents.addEvent);
router.get("/publicEvents/", ctrPublicEvents.getEvents);
router.delete("/publicEvents/:eventId", VerifyToken.verifyToken, ctrPublicEvents.deleteEvent);

router.put('/users/:idUser/event', VerifyToken.verifyToken, ctrlUsers.addEvent);
router.get('/users/:idUser/event', VerifyToken.verifyToken, ctrlUsers.eventAll);
router.delete('/users/:idUser/event/:idEvent', VerifyToken.verifyToken, ctrlUsers.removeEvent);
router.put('/users/:idUser/event/:idEvent', VerifyToken.verifyToken, ctrlUsers.updateEvent);
router.get('/users/:idUser/event/:idEvent', VerifyToken.verifyToken, ctrlUsers.getEventById);

router.put('/users/:idUser/slot', VerifyToken.verifyToken, ctrlUsers.addSlot);
router.get('/users/:idUser/slot', VerifyToken.verifyToken, ctrlUsers.slotAll);
router.delete('/users/:idUser/slot/:idSlot', VerifyToken.verifyToken, ctrlUsers.removeSlot);
router.put('/users/:idUser/slot/:idSlot', VerifyToken.verifyToken, ctrlUsers.updateSlot);

router.put('/users/:idUser/note', VerifyToken.verifyToken, ctrlUsers.addNote);
router.get('/users/:idUser/note', VerifyToken.verifyToken, ctrlUsers.noteAll);
router.delete('/users/:idUser/note/:idNote', VerifyToken.verifyToken, ctrlUsers.removeNote);
router.put('/users/:idUser/note/:idNote', VerifyToken.verifyToken, ctrlUsers.updateNote);

module.exports = router;