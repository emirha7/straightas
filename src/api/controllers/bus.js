const request = require('request');

module.exports.showBus = function(req, res) {
	var url = "https://www.trola.si/";
	var station = encodeURIComponent(req.params.station);
	var routeNumber = req.params.rnumber;
	
	if (routeNumber == null)
		url += station;
	else 
		url += station + "/" + routeNumber;
	
	request({
		url: url,
		json: true
	}, function(error, response, body) {
		if (error) {
			console.log(error);
			res.status(500).send({
				message: "Internal sever error!"
			});
			return;
		}else{
			res.status(200).send({
				message: "OK",
				data: body
			});
		}
	});
}