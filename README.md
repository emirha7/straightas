# Lastni projekt pri predmetu TPO

Vsaka skupina, ki je sestavljena iz 4 �lanov, mora razviti lastni projekt (LP) na izbrani problemski domeni, in sicer od **predloga projekta** do **implementacije**, kjer je podrobna razdelitev naslednja:

* **1. LP** - [Predlog projekta](docs/predlog-projekta),
* **2. LP** - [Zajem zahtev](docs/zajem-zahtev),
* **3. LP** - [Na�rt re�itve](docs/nacrt) in
* **4. LP** - [Implementacija](src).

URL: [https://tpo-ci-cd-team10.herokuapp.com/entry](https://tpo-ci-cd-team10.herokuapp.com/entry)